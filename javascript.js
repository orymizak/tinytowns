var contadorGlobal=0;
var PuntosTotales = 0;
var valor = 0;
var tabla = document.getElementsByClassName("celda");
var material = "";
var botonMaterial = "";
var contador=0;
var contador2 = 0;
var construccion ="";
var numeroConstrucciones = 0;
var estado = false;
var materialesSeleccionados = 0;
var celda1;
var celda2;
var celda3;
var celda4;
var celda5;
var celda6;
var bandera=0;
var TDC =0;
var farm = false;
var granary = false;
var greenhouse = false;
var orchard = false;
var grises = "";
var fountain = false;
var millstone = false;
var shed = false;
var well = false;
var abbey = false;
var chapel = false;
var cloister = false;
var temple = false;
var almshouse = false;
var feastHall = false;
var tavern = false;
var bakery = false;
var market = false;
var tailor = false;
var theater = false;
var factory = false;
var tradingPost = false;
var warehouse = false;
var architectsGuild = false;
var asa = false; //Archive Of The Second Age
var barrettCastle = false;
var coc = false; // CathedralOfCaterina
var groveUniversity = false;
var mandrasPlace = false;
var sotet = false; // Shrine of the Elder Tree
var silvaForum = false;
var theStarloom = false;
var sb = false; //Statue Of The Bondmaker
var monumento = 0;
var totalC=0;
var totalER=0;
var totalEG=0;
var totalEN=0;
var totalEV=0;
var totalEY=0;
var totalEA=0;
var totalM=0;

// contadores globales de edificios
var total_puntos = 0;
var penalización = 0;
var c2_tavern = 0;
var c2_almshouse = 0;
var edificio_contador = 0;
var c_cottage = 0;
var c_orchard = 0;
var c_farm = 0;
var c_granary = 0;
var c_greenhouse = 0;
var c_fountain = 0;
var c_millstone = 0;
var c_shed = 0;
var c_well = 0;
var c_abbey = 0;
var c_chappel = 0;
var c_tample = 0;
var c_cloister = 0;
var c_tavern = 0;
var c_feasthall = 0;
var c_almshouse = 0;
var c_bakery = 0;
var c_theater = 0;
var c_tailor = 0;
var c_market = 0;
var c_warehouse = 0;
var c_tradingpost = 0;
var c_factory = 0;
var c_coc = 0;
var c_asa = 0;



//Crea la matriz
var matrix = new Array(4);
for (i = 0; i <matrix.length; i++){
	matrix[i]= new Array(4);
}

//Pone un listener a las celdas para poder jugar
function inicio() {
	var i = 0;
	while(i < tabla.length){
		tabla[i].addEventListener("click", juego);
		i++;
		}
}


function cargarPagina() {
    window.location.reload();
}

//Imprime el contenido de la matriz
function ImprimirMatrix(){
	for(var x = 0; x < matrix.length; x++){
		for(var y = 0; y < matrix[x].length; y++){
			console.log("["+matrix[x][y]+"]"+" ");
			if(numeroConstrucciones == 0){
				alert("Ya no puedes hacer mas constucciones");
			}
		}
		console.log("\n");
	}
}

//Genera un numero random que despues lo convierte en un material aleatorio
function MaterialRandom() { 
	var material="";
	var aleatorio = Math.floor(Math.random() * (5 - 1 + 1) + 1);
	if(aleatorio<=5 && aleatorio>=1){
		switch (aleatorio) {
			case 1: material="Madera";
			break;
			case 2: material="Trigo";
			break;
			case 3: material="Ladrillo";
			break;
			case 4: material="Vidrio";
			break;
			case 5: material="Piedra";
			break;
		}
		return material;
	}
	//console.log(material);
/*
	else
	{
		MaterialRandom();
	}
	*/
}

//Le da un material al boton1
function cmc1(){
	var boton1 = document.getElementById("boton1");
	boton1.disabled = false;
	boton1.innerText = MaterialRandom();
	document.getElementById("test1").src = "./imagenes/materiales/"+boton1.innerText+".png"; 
	document.getElementById("test1").value = ""+boton1.innerText+""; 

}

//Le da un material al boton2
function cmc2(){
	var boton2 = document.getElementById("boton2");
	boton2.disabled = false;
	boton2.innerText=MaterialRandom();
	document.getElementById("test2").src = "./imagenes/materiales/"+boton2.innerText+".png"; 
	document.getElementById("test2").value = ""+boton2.innerText+""; 
}

//Le da un material al boton3
function cmc3(){
	var boton3 = document.getElementById("boton3");
	boton3.disabled = false;
	boton3.innerText=MaterialRandom();
	document.getElementById("test3").src = "./imagenes/materiales/"+boton3.innerText+".png"; 
	document.getElementById("test3").value = ""+boton3.innerText+""; 
}

//barajear construcciones rojas
function BCR() { 
	var aleatorio = Math.floor(Math.random() * (4 - 1 + 1) + 1);
	if(aleatorio<=4 && aleatorio>=1){
		switch (aleatorio) {
			case 1: 
			farm = true, console.log("Farm");
			document.getElementById("c_1").src = "./imagenes/cartas/c_farm.png"; 
			break;
			case 2: 
			granary = true, console.log("Granary");
			document.getElementById("c_1").src = "./imagenes/cartas/c_granaries.png"; 
			break;
			case 3: 
			greenhouse = true, console.log("Greenhouse");
			document.getElementById("c_1").src = "./imagenes/cartas/c_greenhouse.png"; 
			break;
			case 4: 
			orchard = true, console.log("Orchard");
			document.getElementById("c_1").src = "./imagenes/cartas/c_orchard.png"; 
			break;
		}
	}
}

//barajear construcciones Grises
function BCG() { 
	var aleatorio = Math.floor(Math.random() * (4 - 1 + 1) + 1);
	if(aleatorio<=4 && aleatorio>=1){
		switch (aleatorio) {
			case 1: grises = "Fountain", console.log("Fountain");
			document.getElementById("c_2").src = "./imagenes/cartas/c_fuente.png"; 
			break;
			case 2: grises = "Millstone", console.log("Millstone");
			document.getElementById("c_2").src = "./imagenes/cartas/c_millstone.png"; 
			break;
			case 3: grises = "Shed", console.log("Shed");
			document.getElementById("c_2").src = "./imagenes/cartas/c_shed.png"; 
			break;
			case 4: grises = "Well", console.log("Well");
			document.getElementById("c_2").src = "./imagenes/cartas/c_well.png"; 
			break;
		}
	}
}

//barajear construcciones Narangas 
function BCN() { 
	var aleatorio = Math.floor(Math.random() * (4 - 1 + 1) + 1);
	if(aleatorio<=4 && aleatorio>=1){
		switch (aleatorio) {
			case 1: abbey = true, console.log("Abbey");
			document.getElementById("c_3").src = "./imagenes/cartas/c_abbey.png"; 
			break;
			case 2: chapel = true, console.log("Chapel");
			document.getElementById("c_3").src = "./imagenes/cartas/c_chapel.png"; 
			break;
			case 3: cloister = true, console.log("Cloister");
			document.getElementById("c_3").src = "./imagenes/cartas/c_cloister.png"; 
			break;
			case 4: tample = true, console.log("Tample");
			document.getElementById("c_3").src = "./imagenes/cartas/c_temple.png"; 
			break;
		}
	}
}

//barajear construcciones Verdes 
function BCV() { 
	var aleatorio = Math.floor(Math.random() * (3 - 1 + 1) + 1);
	if(aleatorio<=3 && aleatorio>=1){
		switch (aleatorio) {
			case 1: almshouse = true, console.log("Almshouse");
			document.getElementById("c_4").src = "./imagenes/cartas/c_almshouse.png"; 
			break;
			case 2: feastHall = true, console.log("FeastHall");
			document.getElementById("c_4").src = "./imagenes/cartas/c_feast_hall.png"; 
			break;
			case 3: tavern = true, console.log("Tavern");
			document.getElementById("c_4").src = "./imagenes/cartas/c_tavern.png"; 
			break;
		}
	}
}

//barajear construcciones Amarillas 
function BCY() { 
	var aleatorio = Math.floor(Math.random() * (4 - 1 + 1) + 1);
	if(aleatorio<=4 && aleatorio>=1){
		switch (aleatorio) {
			case 1: bakery = true, console.log("Bakery");
			document.getElementById("c_5").src = "./imagenes/cartas/c_bakery.png"; 
			break;
			case 2: market = true, console.log("Market");
			document.getElementById("c_5").src = "./imagenes/cartas/c_market.png"; 
			break;
			case 3: tailor = true, console.log("Tailor");
			document.getElementById("c_5").src = "./imagenes/cartas/c_tailor.png"; 
			break;
			case 4: theater = true, console.log("theater");
			document.getElementById("c_5").src = "./imagenes/cartas/c_theater.png"; 
			break;
		}
	}
}

//barajear construcciones Azules 
function BCA() { 
	var aleatorio = Math.floor(Math.random() * (3 - 1 + 1) + 1);
	if(aleatorio<=3 && aleatorio>=1){
		switch (aleatorio) {
			case 1: factory = true, console.log("Factory");
			document.getElementById("c_6").src = "./imagenes/cartas/c_factory.png"; 
			break;
			case 2: tradingPost = true, console.log("Trading Post");
			document.getElementById("c_6").src = "./imagenes/cartas/c_trading_post.png"; 
			break;
			case 3: warehouse = true, console.log("Warehouse");
			document.getElementById("c_6").src = "./imagenes/cartas/c_warehouse.png"; 
			break;
		}
	}
}

//barajear Monumentos /solo tenemos dos
function BCM() {
	var aleatorio = Math.floor(Math.random() * (10 - 1 + 1) + 1);
	var aleatorio2 = Math.floor(Math.random() * (10 - 1 + 1) + 1);
	if (aleatorio != aleatorio2) {
	if(aleatorio<=10 && aleatorio>=1){
		switch (aleatorio) {
			case 1: ArchitectsGuild = true;
			break;
			case 2: Asa = true; //Archive Of The Second Age
			break;
			case 3: BarrettCastle = true;
			break;
			case 4: Coc = true; // CathedralOfCaterina
 			break;
			case 5: GroveUniversity = true;
			break;
			case 6: MandrasPlace = true;
			break;
			case 7: Sotet = true; // Shrine of the Elder Tree
			break;
			case 8: SilvaForum = true;
			break;
			case 9: TheStarloom = true;
			break;
			case 10: Sb = true; //Statue Of The Bondmaker
		}
	}
	if(aleatorio2<=10 && aleatorio>=1 && aleatorio2!=aleatorio){
		switch (aleatorio2) {
			case 1: ArchitectsGuild = true;
			break;
			case 2: Asa = true; //Archive Of The Second Age
			break;
			case 3: BarrettCastle = true;
			break;
			case 4: Coc = true; // CathedralOfCaterina
 			break;
			case 5: GroveUniversity = true;
			break;
			case 6: MandrasPlace = true;
			break;
			case 7: Sotet = true; // Shrine of the Elder Tree
			break;
			case 8: SilvaForum = true;
			break;
			case 9: TheStarloom = true;
			break;
			case 10: Sb = true; //Statue Of The Bondmaker
		}
	}
}
else	
	{
		BCM();
	}
}

//Regresar del modo constructor
function Atras() {
		document.getElementById("MENSAJE").innerHTML = "Seleccione un material."; 

	var i = 0;
	var j = 0;
	var boton1 = document.getElementById("boton1");
	var boton2 = document.getElementById("boton2");
	var boton3 = document.getElementById("boton3");

	var test1 = document.getElementById("test1");
	var test2 = document.getElementById("test2");
	var test3 = document.getElementById("test3");

	var botonA = document.getElementById("botonA");
	var botonMC = document.getElementById("botonMC");
	var botonOK = document.getElementById("botonOK");
	estado = false;
	botonA.disabled = true;
	boton1.disabled = false;
	boton2.disabled = false;
	boton3.disabled = false;

	test1.disabled = false;
	test2.disabled = false;
	test3.disabled = false;

	// restaura los elementos seleccionados en modo construir si se sale, evita bug
	celda1 = undefined;
	celda2 = undefined;
	celda3 = undefined;
	celda4 = undefined;
	celda5 = undefined;
	celda6 = undefined;
	botonOK.disabled = true;

	contador2 = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	while(j < tabla.length){
		tabla[j].addEventListener("click",juego);
		j++;
	}
	if(numeroConstrucciones >= 1){
	botonMC.disabled = false;
	}
	else
	{
		botonMC.disabled = true;
	} //comentarioVAR

}

// ejecuta la validacion de los materiales para despues construir en caso de ser posible
function Aceptar() {
	var botonOK = document.getElementById("botonOK");
	botonOK.disabled=true;
	document.getElementById("MENSAJE").innerHTML = "...?"; 

	if(contador2 >= 2){
		Edificaciones();
	}
}

//Toma el material que esten en alguno de los botones 1,2 o 3 para luego pasarlo a la variable material
function TomarMaterial(){
	var boton1 = document.getElementById("boton1");
	var boton2 = document.getElementById("boton2");
	var boton3 = document.getElementById("boton3");

	var test1 = document.getElementById("test1").value;
	var test2 = document.getElementById("test2").value;
	var test3 = document.getElementById("test3").value;

	var idBoton = event.target.id;
	var boton = document.getElementById(idBoton);

	material = boton.value;
	botonMaterial = idBoton;
	boton.innerText="";
	
	document.getElementById("MENSAJE").innerHTML = "Material seleccionado: "+material+"."; 
	console.log(boton.id);

	document.getElementById(""+boton.id+"").src = "./imagenes/materiales/"+material+"_1.png"; 
	var comparar = boton.id;

	switch(comparar){
		case "test1": 
		document.getElementById("test2").src = "./imagenes/materiales/"+test2+".png"; 
		document.getElementById("test3").src = "./imagenes/materiales/"+test3+".png"; 
		break;
		case'test2': 
		document.getElementById("test1").src = "./imagenes/materiales/"+test1+".png"; 
		document.getElementById("test3").src = "./imagenes/materiales/"+test3+".png"; 
		break;
		case'test3': 
		document.getElementById("test2").src = "./imagenes/materiales/"+test2+".png"; 
		document.getElementById("test1").src = "./imagenes/materiales/"+test1+".png"; 
		break;
	}
}
//No indagar mas en el codigo que no sabe que hay dentro de el 
function TomarMaterial2(){

	// función inhabilitada, para la posteridad
	//var boton1 = document.getElementById("boton1");
	//var boton2 = document.getElementById("boton2");
	//var boton3 = document.getElementById("boton3");
	// var idBoton = event.target.id;
	// var boton = document.getElementById(idBoton);
	// material=boton.textContent;
	// botonMaterial=idBoton;
	// boton.innerText="";
	// var comparar = idBoton;
	// derogado tmb, innecesario br0
	//
	// switch(comparar){
	// 	case "boton1": boton2.disabled = true, boton3.disabled = true
	// 	break;
	// 	case'boton2': boton1.disabled = true, boton3.disabled = true
	// 	break;
	// 	case'boton3': boton1.disabled = true, boton2.disabled = true
	// 	break;
	// }
}

//La funcion de juego tiene el procedimineto para juegar desde mandar los iconos a cada celda, llenar la matrix con materiales y una
// que otra validacion para poder jugar (Desabilitar botones etc)
function juego() {
	var espacioLibre = 0;
	var i = 0;
	var idBoton = event.target.id;
	var celda = document.getElementById(idBoton);
	var idfantasma = idBoton;
	var cordenadaX = idBoton[1];
	var cordenadaY = idBoton[2];
	var boton1 = document.getElementById("boton1");
	var boton2 = document.getElementById("boton2");
	var boton3 = document.getElementById("boton3");
	if(celda == null || celda.value != undefined){
		console.log("No se pude poner aqui");
		document.getElementById("MENSAJE").innerHTML = "Lo siento, no es posible modificar o cambiar un material/construcción."; 
	}
	else
	{
	switch (material) {
		case 'Madera': valor:celda.innerHTML="<img id="+idfantasma+"  src='./imagenes/materiales/Madera.png'  width='90%'height='90%'>", celda.value = "Madera";
		document.getElementById("MENSAJE").innerHTML = "Material colocado: Madera."; 
		contadorGlobal++;
		console.log("-------------"); 
		console.log(contadorGlobal); 
		console.log("-------------");
		break;
		case 'Trigo': valor:celda.innerHTML="<img id="+idfantasma+" src='./imagenes/materiales/Trigo.png'  width='90%'height='90%'>", celda.value = "Trigo";
		document.getElementById("MENSAJE").innerHTML = "Material colocado: Trigo."; 
		contadorGlobal++;
		console.log("-------------"); 
		console.log(contadorGlobal); 
		console.log("-------------");
		break;
		case 'Ladrillo': valor:celda.innerHTML="<img id="+idfantasma+"  src='./imagenes/materiales/Ladrillo.png'  width='90%'height='90%'>", celda.value = "Ladrillo";
		document.getElementById("MENSAJE").innerHTML = "Material colocado: Ladrillo."; 
		contadorGlobal++;
		console.log("-------------"); 
		console.log(contadorGlobal); 
		console.log("-------------");
		break;
		case 'Vidrio': valor:celda.innerHTML="<img id="+idfantasma+"  src='./imagenes/materiales/Vidrio.png'  width='90%'height='90%'>", celda.value = "Vidrio";
		document.getElementById("MENSAJE").innerHTML = "Material colocado: Vidrio."; 
		contadorGlobal++;
		console.log("-------------"); 
		console.log(contadorGlobal); 
		console.log("-------------");
		break;
		case 'Piedra': valor:celda.innerHTML="<img id="+idfantasma+" src='./imagenes/materiales/Piedra.png'  width='90%'height='90%'>", celda.value = "Piedra";
		document.getElementById("MENSAJE").innerHTML = "Material colocado: Piedra."; 
		contadorGlobal++;
		console.log("-------------"); 
		console.log(contadorGlobal); 
		console.log("-------------");
		break;
	}


	switch (material) {
		case 'Madera': matrix[cordenadaX][cordenadaY]="Madera";
		break;
		case 'Trigo': matrix[cordenadaX][cordenadaY]="Trigo";
		break;
		case 'Ladrillo': matrix[cordenadaX][cordenadaY]="Ladrillo";
		break;
		case 'Vidrio': matrix[cordenadaX][cordenadaY]="Vidrio";
		break;
		case 'Piedra': matrix[cordenadaX][cordenadaY]="Piedra";
		break;
	}


	var h = document.getElementById(idBoton)
	if(material == undefined){
		console.log("Tome un material de los botones de abajo.");
		document.getElementById("MENSAJE").innerHTML = "Seleccione un material de abajo para continuar."; 
		document.getElementById("test").src = "./imagenes/íconos/question.png"; 

	}
	else
	{
		switch (botonMaterial) {
			case 'test1': cmc1()
			break;
			case 'test2': cmc2()
			break;
			case 'test3': cmc3() 
			break;
		}
		boton1.disabled = false;
		boton2.disabled = false;
		boton3.disabled = false;
	}
	material = undefined;
	contador++;
	Edificaciones();
	if (numeroConstrucciones>=1) {
		var botonMC = document.getElementById("botonMC");
		botonMC.disabled = false;
	}

	var i = 0;

	while(i < tabla.length){
		if (tabla[i].value == undefined) {
		espacioLibre++;	
		}
			i++;
		}

	if (espacioLibre == 0) {
		boton1.disabled = true;
		boton2.disabled = true;
		boton3.disabled = true;
		document.getElementById("MENSAJE").innerHTML = "Construye para liberar espacio."; 
	}
	else
	{
		boton1.disabled = false;
		boton2.disabled = false;
		boton3.disabled = false;
	}
	Fin();
}


}

function QuitarMateriales(id){ 
	var celda = document.getElementById("id");
	celda.innerHTML="";
}

function Terminar(){
	if(numeroConstrucciones == 0){
		while(i < tabla.length){
			tabla[i].removeEventListener("click", juego);
			i++;
		}
	}
	else
	{

	}

}







//estan todas las edificaciones para validar su existencia, ademas si el modo constructor esta activado pasa a la parte de construir una casa
function Edificaciones(){
	if (estado == false) {
		BuscarCottage();
		BEG();
		
		//Edificios Rojos
		BuscarFarm();
		BuscarGranary();
		BuscarGreenhouse();
		BuscarOrchard();

		//Edificios Naranjas
		BuscarAbbey();
		BuscarChapel();
		BuscarCloister();
		BuscarTemple();

		//Edificios Verdes
		BuscarAlmshouse();
		BuscarFeastHall();
		BuscarTavern();

		//Edificios Amarillos (Y)
		BuscarBakery();
		BuscarMarket();
		BuscarTailor();
		BuscarTheater();
		
		//Edificios Azules
		BuscarTradingPost();
		BuscarWarehouse();
		BuscarFactory();

		//Monumentos
		BuscarCOC();
		BuscarASA();
	}
	if(contador2 == 2 && estado == true){
		EG();	
	}	
	if(contador2 == 3 && estado == true){
		Cottage();
		Almshouse();
		FeastHall();
		Tavern();
		COC();	
	}
	if(contador2 == 4 && estado == true){
		Farm();
		Granary();
		Greenhouse();
		Orchard();
		Abbey();
		Chapel();
		Cloister();
		Temple();
		Bakery();
		Market();
		Tailor();
		Theater();
		ASA();
		
	}
	if(contador2 == 5 && estado == true){
		TradingPost();
		Warehouse();
		Factory();
	}
	if(contador2 == 6 && estado == true){
	}
	numeroConstrucciones = totalC+totalEA+totalEG+totalEN+totalER+totalEV+totalEY+totalM;
}

//Ordena los materiales, determina si estan en orden y guarda una casa en construccion y cambia los espacios para poner una casa
function BuscarCottage(){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[c][d] == "Ladrillo")||
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[c][b] == "Ladrillo")||
				(matrix[c][d] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[a][b] == "Ladrillo")||
				(matrix[c][b] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[a][d] == "Ladrillo")||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[c][b] == "Ladrillo")||
				(matrix[a][b] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[c][d] == "Ladrillo")||
				(matrix[c][b] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[a][d] == "Ladrillo")||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[a][b] == "Ladrillo")
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	totalC=cantidad;
}
}

function Cottage(){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var material1;
	var material2;
	var material3;

	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined){
		switch (materialFantasma1.value) {
			//comentarioxY
			case 'Trigo': material1 = document.getElementById(celda1);
			break;
			case 'Vidrio': material2 = document.getElementById(celda1);
			break;
			case 'Ladrillo': material3 = document.getElementById(celda1);
			break;
			default: problema = true;
			break;
		}
		switch (materialFantasma2.value) {
			case 'Trigo': material1 = document.getElementById(celda2);
			break;
			case 'Vidrio': material2 = document.getElementById(celda2);
			break;
			case 'Ladrillo': material3 = document.getElementById(celda2);
			break;
			default: problema = true;
			break;
		}
		switch (materialFantasma3.value) {
			case 'Trigo': material1 = document.getElementById(celda3);
			break;
			case 'Vidrio': material2 = document.getElementById(celda3);
			break;
			case 'Ladrillo': material3 = document.getElementById(celda3);
			break;
			default: problema = true;
			break;
		}
	}
	else
	{
		problema = true
	}
	if(problema == false && material1 != undefined && material2 != undefined && material3 != undefined){	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value)||
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value)||
				(matrix[c][d] == material1.value && matrix[a][d] == material2.value && matrix[a][b] == material3.value)||
				(matrix[c][b] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value)||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[c][b] == material3.value)||
				(matrix[a][b] == material1.value && matrix[a][d] == material2.value && matrix[c][d] == material3.value)||
				(matrix[c][b] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value)||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][b] == material3.value)
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
				}
				construccion="Cottage";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir);
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}   	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled = false;
	}
}

//tomar los materiales de las celdas TMC
function TMC(){
	var id = event.target.id;
	var seleccion = document.getElementById(id);
	if(seleccion == null || seleccion.value == undefined){
		document.getElementById("MENSAJE").innerHTML = "No se puede construir."; 
	}
	else
	{
	//var botonA = document.getElementById("botonA");
	//botonA.disabled = true;
	contador2++;
	// selección3 = selección.value;
	// selección4 = selección.value;
	// selección5 = selección.value;
	// selección6 = selección.value;


	switch (contador2) {
		case 1: celda1 = event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Elementos seleccionados: 1."; 
			break;
		case 2: celda2 = event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Elementos seleccionados: 2."; 

		if (celda1 == celda2)
		{
			document.getElementById("MENSAJE").innerHTML = "Lo siento, no es posible seleccionar el mismo material para construir; reintente."; 
			contador2 = 0;
			var botonOK = document.getElementById("botonOK");
			botonOK.disabled = true;
		
		}
			break;
		case 3: celda3=event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Elementos seleccionados: 3."; 

		if (celda2 == celda3 || celda3 == celda1)
		{
			document.getElementById("MENSAJE").innerHTML = "Lo siento, no es posible seleccionar el mismo material para construir; reintente."; 
			contador2 = 0;
			var botonOK = document.getElementById("botonOK");
			botonOK.disabled = true;
		}
			
			break;
		case 4: celda4=event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Elementos seleccionados: 4."; 
		if (celda4 == celda3 || celda4 == celda2 || celda4 == celda1)
		{
			document.getElementById("MENSAJE").innerHTML = "Lo siento, no es posible seleccionar el mismo material para construir; reintente."; 
			contador2 = 0;
			var botonOK = document.getElementById("botonOK");
			botonOK.disabled = true;

		}
			break;

		case 5: celda5=event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Elementos seleccionados: 5. Tope límite."; 
		if (celda5 == celda4 || celda5 == celda3 || celda5 == celda2 || celda5 == celda1)
		{
			document.getElementById("MENSAJE").innerHTML = "Lo siento, no es posible seleccionar el mismo material para construir; reintente."; 
			contador2 = 0;
			var botonOK = document.getElementById("botonOK");
			botonOK.disabled = true;
		}
			break;

		case 6: celda6=event.target.id;
		document.getElementById("MENSAJE").innerHTML = "Demasiados elementos seleccionados, reintente."; 
		contador2 = 0;
		var botonOK = document.getElementById("botonOK");
		botonOK.disabled = true;
			break;
	}

	if(contador2 >= 2){
		var botonOK = document.getElementById("botonOK");
		botonOK.disabled = false;
	}
}

}

//cambia el Listener de las celdas para tomar los materiales y poder construir
function ModoConstructor(){
	var i = 0;
	var j = 0;
	var boton1 = document.getElementById("boton1");
	var boton2 = document.getElementById("boton2");
	var boton3 = document.getElementById("boton3");
	var test1 = document.getElementById("test1");
	var test2 = document.getElementById("test2");
	var test3 = document.getElementById("test3");
	var botonA = document.getElementById("botonA");
	var botonMC = document.getElementById("botonMC");
	if(numeroConstrucciones == 0){
		document.getElementById("MENSAJE").innerHTML = "A&uacute;n no puede construir."; 

	}
	else
	{
		if(estado == false){
		document.getElementById("MENSAJE").innerHTML = "Entr&oacute; en modo constructor."; 
			// alert("Entro en modo constructor");
		}
		botonA.disabled = false;
		estado = true;
		boton1.disabled=true;
		boton2.disabled=true;
		boton3.disabled=true;
		test1.disabled=true;
		test2.disabled=true;
		test3.disabled=true;
		while(i < tabla.length){
			tabla[i].removeEventListener("click", juego);
			i++;
		}
		while(j < tabla.length){
			tabla[j].addEventListener("click", TMC);
			j++;
		}
		botonMC.disabled=true;
	}
}

//proceso
function Construir() {
	var idBoton = event.target.id;
	var celda = document.getElementById(idBoton);
	var j=0;
	var i=0;
	matrix[celda.id[1]][celda.id[2]]=construccion;
	switch (construccion) {
		case 'Cottage': celda.innerHTML="<img src='./imagenes/edificios/Cottage.png' width='90%'height='90%'>";
				contadorGlobal--;
				contadorGlobal--;
				c_cottage++;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Farm': celda.innerHTML="<img src='./imagenes/edificios/Farm.png'  width='90%'height='90%'>";
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				c_farm++;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Granary': celda.innerHTML="<img src='./imagenes/edificios/Granaries.png'  width='90%'height='90%'>";
				c_granary++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Orchard': celda.innerHTML="<img src='./imagenes/edificios/Orchard.png'  width='90%'height='90%'>";
				c_orchard++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Greenhouse': celda.innerHTML="<img src='./imagenes/edificios/green_house.png'  width='90%'height='90%'>";
				c_greenhouse++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Fountain': celda.innerHTML="<img src='./imagenes/edificios/fuente.png'  width='90%'height='90%'>";
				c_fountain++;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Millstone': celda.innerHTML="<img src='./imagenes/edificios/millstone.png'  width='90%'height='90%'>";
				c_millstone++;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Shed': celda.innerHTML="<img src='./imagenes/edificios/shed.png'  width='90%'height='90%'>";
				c_shed++;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Well': celda.innerHTML="<img src='./imagenes/edificios/well.png'  width='90%'height='90%'>";
				c_well++;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Abbey': celda.innerHTML="<img src='./imagenes/edificios/abbey.png'  width='90%'height='90%'>";
				c_abbey++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Chapel': celda.innerHTML="<img src='./imagenes/edificios/chapel.png'  width='90%'height='90%'>";
				c_chappel++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Cloister': celda.innerHTML="<img src='./imagenes/edificios/cloister.png'  width='90%'height='90%'>";
				c_cloister++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Tample': celda.innerHTML="<img src='./imagenes/edificios/temple.png'  width='90%'height='90%'>";
				c_tample++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Almshouse': celda.innerHTML="<img src='./imagenes/edificios/almshouse.png'  width='90%'height='90%'>";
				c_almshouse++;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'FeastHall': celda.innerHTML="<img src='./imagenes/edificios/feast_hall.png'  width='90%'height='90%'>";
				c_feasthall++;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Tavern': celda.innerHTML="<img src='./imagenes/edificios/tavern.png'  width='90%'height='90%'>";
				c_tavern++;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Bakery': celda.innerHTML="<img src='./imagenes/edificios/bakery.png'  width='90%'height='90%'>";
				c_bakery++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Market': celda.innerHTML="<img src='./imagenes/edificios/market.png'  width='90%'height='90%'>";
				c_market++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Tailor': celda.innerHTML="<img src='./imagenes/edificios/tailor.png'  width='90%'height='90%'>";
				c_tailor++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Theater': celda.innerHTML="<img src='./imagenes/edificios/theater.png'  width='90%'height='90%'>";
				c_theater++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Factory': celda.innerHTML="<img src='./imagenes/edificios/factory.png'  width='90%'height='90%'>";
				c_factory++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Trading Post': celda.innerHTML="<img src='./imagenes/edificios/trading_post.png'  width='90%'height='90%'>";
				c_tradingpost++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'Warehouse': celda.innerHTML="<img src='./imagenes/edificios/warehouse.png'  width='90%'height='90%'>";
				c_warehouse++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'CathedralOfCaterina': celda.innerHTML="<img src='./imagenes/arquitecturas/coc.png'  width='90%'height='90%'>";
				c_coc++;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
		case 'ASA': celda.innerHTML="<img src='./imagenes/arquitecturas/asa.png'  width='90%'height='90%'>";
				c_asa++;
				contadorGlobal--;
				contadorGlobal--;
				contadorGlobal--;
				console.log("--------------A");
				console.log(contadorGlobal);
				console.log("--------------A");
		break;
	}

	estado = false;
	Edificaciones();
	estado = true;
	var botonA = document.getElementById("botonA");
	botonA.disabled = false;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",Construir);
		i++;
	}
	while(j < tabla.length){
		tabla[j].addEventListener("click", TMC);
		j++;
	}
	Atras();
}

//Ordena los materiales, determina si estan en orden y guarda una casa en construccion y cambia los espacios para poner un edifcio gris
function EG(){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var material1;
	var material2;
	var problema = false;
	contador2 = 0;
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Madera': material2 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': material1 = document.getElementById(celda2);
		break;
		case 'Madera': material2 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	if(problema == false && material1!= undefined && material2!= undefined){	
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		for(var x = 0; x <= matrix.length-1; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value &&
						material1.id[1] == a && material1.id[2] == d && material2.id[1] == a && material2.id[2] == b ) || 
					(matrix[a][d] == material2.value && matrix[a][b] == material1.value &&
						material1.id[1] == a && material1.id[2] == b && material2.id[1] == a && material2.id[2] == d) ){
					matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				var i = 0;
				while(i < tabla.length){
				tabla[i].removeEventListener("click",TMC);
				i++;
				}
				construccion=grises;
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				}
				else
				{
				}
				if(b == 4){
				d = 0;
				b = 1;
			}
			else
			{
				b++;
				d++;
			}
		}
		a++;
		c++;
	}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-1; y++){
			if(
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value &&
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == c && material2.id[2] == d) || 
				(matrix[a][d] == material2.value && matrix[c][d] == material1.value &&
					material1.id[1] == c && material1.id[2] == d && material2.id[1] == a && material2.id[2] == d) ){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				var i = 0;
				while(i < tabla.length){
				tabla[i].removeEventListener("click",TMC);
				i++;
				}
				construccion=grises;
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
			}
			else
			{
			}
			if(b == 4){
			d = 0;
			b = 1;
		}
		else
		{
			b++;
			d++;
		}
	}
	a++;
	c++;
}                      	
	}
	else
	{
		
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}
//buscar edificios grises
function BEG(){
	var cantidad=0;	
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		for(var x = 0; x <= matrix.length-1; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if((matrix[a][d] == "Madera" && matrix[a][b] == "Piedra") || (matrix[a][d] == "Piedra" && matrix[a][b] == "Madera") ){
					cantidad++;
				}
				else
				{
				}
				if(b == 4){
				d = 0;
				b = 1;
			}
			else
			{
				b++;
				d++;
			}
		}
		a++;
		c++;
	}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-1; y++){
			if((matrix[a][d] == "Madera" && matrix[c][d] == "Piedra") || (matrix[a][d] == "Piedra" && matrix[c][d] == "Madera") ){
				cantidad++;
			}
			else
			{
			}
			if(b == 4){
			d = 0;
			b = 1;
		}
		else
		{
			b++;
			d++;
		}
	}
	a++;
	c++;
}
	if (estado == false){
		totalEG=cantidad
	}

}

//Eficicios rojos flataa por checar si tiene algun fallo 
//CHECAR
function Farm(){
	if(farm == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Trigo':material1 = document.getElementById(celda1);
		break;
		case 'Madera': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Madera': if(material3 == undefined){
			material3 = document.getElementById(celda2);
		}
		else
		{
			material4 = document.getElementById(celda2);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Madera': if(material3 == undefined){
			material3 = document.getElementById(celda3);
		}
		else
		{
			material4 = document.getElementById(celda3);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material2 = document.getElementById(celda4);
		}
		break;
		case 'Madera': if(material3 == undefined){
			material3 = document.getElementById(celda4);
		}
		else
		{
			material4 = document.getElementById(celda4);
		}
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
				material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d &&
				material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b ) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
				material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b &&
				material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d &&
				material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d &&
				material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d) 
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				matrix[material4.id[1]][material4.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				material4.innerHTML="";
				material4.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
				}
				construccion="Farm";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir); 
				material4.addEventListener("click",Construir);
			}
			else
			{
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;                     	
	}
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}
function BuscarFarm(){
	if(farm == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][d] == "Madera" && matrix[c][b] == "Madera") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[a][b] == "Madera" && matrix[c][b] == "Madera") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Madera" && matrix[a][b] == "Madera") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Madera" && matrix[c][d] == "Madera") ||
				
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][b] == "Madera" && matrix[c][d] == "Madera") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[c][d] == "Madera" && matrix[a][d] == "Madera") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Madera" && matrix[a][b] == "Madera") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[a][b] == "Madera" && matrix[c][b] == "Madera")
				
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){

	totalER=cantidad; 
}
}
}

function BuscarGranary(){
	if(granary == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][d] == "Madera" && matrix[c][b] == "Ladrillo") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[c][b] == "Madera" && matrix[a][b] == "Ladrillo") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][b] == "Madera" && matrix[a][d] == "Ladrillo") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Madera" && matrix[c][d] == "Ladrillo") ||
				
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][b] == "Madera" && matrix[c][d] == "Ladrillo") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[c][d] == "Madera" && matrix[a][d] == "Ladrillo") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Madera" && matrix[a][b] == "Ladrillo") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[a][b] == "Madera" && matrix[c][b] == "Ladrillo")
				
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	totalER=cantidad; 
}
}
}
function Granary(){
	if(granary == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Trigo':material1 = document.getElementById(celda1);
		break;
		case 'Madera': material3 = document.getElementById(celda1);
		break;
		case 'Ladrillo': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Madera': material3 = document.getElementById(celda2);
		break;
		case 'Ladrillo': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Madera': material3 = document.getElementById(celda3);
		break;
		case 'Ladrillo': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material2 = document.getElementById(celda4);
		}
		break;
		case 'Madera': material3 = document.getElementById(celda4);
		break;
		case 'Ladrillo': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == c && material3.id[2] == d && material4.id[1] == c && material4.id[2] == b) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[c][b] == material3.value && matrix[a][b] == material4.value&&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d &&
					material3.id[1] == c && material3.id[2] == b && material4.id[1] == a && material4.id[2] == b) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][b] == material3.value && matrix[a][d] == material4.value&&
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == a && material3.id[2] == b && material4.id[1] == a && material4.id[2] == d) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value&&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == a && material3.id[2] == d && material4.id[1] == c && material4.id[2] == d) ||
				
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value && matrix[c][d] == material4.value&&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == c && material3.id[2] == b && material4.id[1] == c && material4.id[2] == d) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value&&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == c && material3.id[2] == d && material4.id[1] == a && material4.id[2] == d) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value&&
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b &&
					material3.id[1] == a && material3.id[2] == d && material4.id[1] == a && material4.id[2] == b) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value&&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d &&
					material3.id[1] == a && material3.id[2] == b && material4.id[1] == c && material4.id[2] == b) 
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				matrix[material4.id[1]][material4.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				material4.innerHTML="";
				material4.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
				}
				construccion="Granary";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir); 
				material4.addEventListener("click",Construir);
			}
			else
			{
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;                     	
	}
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}

function BuscarGreenhouse(){
	if(greenhouse == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				

				(matrix[a][d] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[c][d] == "Madera" && matrix[c][b] == "Madera") ||
				(matrix[c][d] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[c][b] == "Madera" && matrix[a][b] == "Madera") ||
				(matrix[c][b] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[a][b] == "Madera" && matrix[a][d] == "Madera") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[a][d] == "Madera" && matrix[c][d] == "Madera") ||
				
				(matrix[a][b] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[c][b] == "Madera" && matrix[c][d] == "Madera") ||
				(matrix[c][b] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[a][d] == "Madera" && matrix[c][d] == "Madera") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[a][d] == "Madera" && matrix[a][b] == "Madera") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[a][b] == "Madera" && matrix[c][b] == "Madera")
				
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	totalER=cantidad;
}
}
}
function Greenhouse(){
	if(greenhouse == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Trigo':material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Madera': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': material1 = document.getElementById(celda2);
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Madera': if (material3 == undefined){
			material3 = document.getElementById(celda2);
		}
		else
		{
			material4 = document.getElementById(celda2);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': material1 = document.getElementById(celda3);
		break;
		case 'Vidrio': material2 = document.getElementById(celda3);
		break;
		case 'Madera': if (material3 == undefined){
			material3 = document.getElementById(celda3);
		}
		else
		{
			material4 = document.getElementById(celda3);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Trigo': material1 = document.getElementById(celda4);
		break;
		case 'Vidrio': material2 = document.getElementById(celda4);
		break;
		case 'Madera': if (material3 == undefined){
			material3 = document.getElementById(celda4);
		}
		else
		{
			material4 = document.getElementById(celda4);
		}
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == a && material2.id[2] == b &&
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d && 
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b) ||
				(matrix[c][d] == material1.value && matrix[a][d] == material2.value && matrix[c][b] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] == c && material1.id[2] == d && material2.id[1] == a && material2.id[2] == d &&
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b && 
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b) ||
				(matrix[c][b] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[a][d] == material4.value &&
					material1.id[1] == c && material1.id[2] == b && material2.id[1] == c && material2.id[2] == d &&
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b && 
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] == a && material1.id[2] == b && material2.id[1] == c && material2.id[2] == b &&
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d && 
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d) ||
				
				(matrix[a][b] == material1.value && matrix[a][d] == material2.value && matrix[c][b] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] == a && material1.id[2] == b && material2.id[1] == a && material2.id[2] == d &&
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b && 
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d) ||
				(matrix[c][b] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] == c && material1.id[2] == b && material2.id[1] == a && material2.id[2] == b &&
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d && 
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == d) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] == c && material1.id[2] == d && material2.id[1] == c && material2.id[2] == b &&
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == d && 
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == a && material2.id[2] == d &&
					material3.id[1]||material4.id[1] == a && material3.id[2]||material4.id[2] == b && 
					material3.id[1]||material4.id[1] == c && material3.id[2]||material4.id[2] == b)
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				matrix[material4.id[1]][material4.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				material4.innerHTML="";
				material4.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
				}
				construccion="Greenhouse";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir); 
				material4.addEventListener("click",Construir);
			}
			else
			{
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;                     	
	}
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}

function Orchard(){
	if(orchard == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Piedra':material1 = document.getElementById(celda1);
		break;
		case 'Trigo': material2 = document.getElementById(celda1);
		break;
		case 'Madera': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': material1 = document.getElementById(celda2);
		break;
		case 'Trigo': if(material2 == undefined){
			material2 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Madera':	material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Piedra': material1 = document.getElementById(celda3);
		break;
		case 'Trigo': if(material2 == undefined){
			material2 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Madera':	material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Piedra': material1 = document.getElementById(celda4);
		break;
		case 'Trigo': if(material2 == undefined){
			material2 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Madera':	material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}

	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] == a && material1.id[2] == d &&
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == b &&
					material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == d &&
					material4.id[1] == c && material4.id[2] == b ) ||
				(matrix[c][d] == material1.value && matrix[a][d] == material2.value && matrix[c][b] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] == c && material1.id[2] == d &&
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == d &&
					material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == b &&
					material4.id[1] == a && material4.id[2] == b ) ||
				(matrix[c][b] == material1.value && matrix[a][b] == material2.value && matrix[a][b] == material3.value && matrix[a][d] == material4.value &&
					material1.id[1] == c && material1.id[2] == b &&
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == b &&
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == b &&
					material4.id[1] == a && material4.id[2] == d ) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] == a && material1.id[2] == b &&
					material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == b &&
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == d &&
					material4.id[1] == c && material4.id[2] == d )
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				matrix[material4.id[1]][material4.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				material4.innerHTML="";
				material4.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
				}
				construccion="Orchard";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir); 
				material4.addEventListener("click",Construir);
			}
			else
			{
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;                     	
	}
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}
function BuscarOrchard(){
	if(orchard == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Trigo" && matrix[c][d] == "Trigo" && matrix[c][b] == "Madera") ||
				(matrix[c][d] == "Piedra" && matrix[a][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][b] == "Madera") ||
				(matrix[c][b] == "Piedra" && matrix[a][b] == "Trigo" && matrix[a][b] == "Trigo" && matrix[a][d] == "Madera") ||
				(matrix[a][b] == "Piedra" && matrix[c][b] == "Trigo" && matrix[a][d] == "Trigo" && matrix[c][d] == "Madera")  
				
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	totalER=cantidad;

}
}
}

//Edificios naranjas
function BuscarAbbey(){
	if(abbey == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[c][e] == "Piedra" && matrix[a][e] == "Vidrio")||
				(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[a][e] == "Piedra" && matrix[c][e] == "Vidrio")||
				(matrix[c][e] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[c][d] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][e] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[a][b] == "Piedra" && matrix[c][d] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{

			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][d] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[c][d] == "Piedra" && matrix[a][b] == "Vidrio")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[e][d] == "Piedra" && matrix[e][b] == "Vidrio")||
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[e][b] == "Piedra" && matrix[e][d] == "Vidrio")||
				(matrix[e][b] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[c][b] == "Piedra" && matrix[a][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEN=cantidad;
}
}
}
function Abbey(){
	if(abbey == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Ladrillo':material1 = document.getElementById(celda1);
		break;
		case 'Piedra': material2 = document.getElementById(celda1);
		break;
		case 'Vidrio': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	
	switch (materialFantasma2.value) {
		case 'Ladrillo':material1 = document.getElementById(celda2);
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	
	switch (materialFantasma3.value) {
		case 'Ladrillo':material1 = document.getElementById(celda3);
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}

	switch (materialFantasma4.value) {
		case 'Ladrillo':material1 = document.getElementById(celda4);
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][e] == material4.value &&
					material1.id[1] == c && material1.id[2] == d && material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == b && 
					material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == e 
					&& material4.id[1] == a && material4.id[2] == e)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][e] == material4.value && 
					material1.id[1] == a && material1.id[2] == d && material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == b && 
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == e 
					&& material4.id[1] == c && material4.id[2] == e)||
					(matrix[c][e] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value && 
					material1.id[1] == c && material1.id[2] == e && material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == b && 
					material2.id[1]||material3.id[1] == c && material2.id[2]||material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == d)||
					(matrix[a][e] == material1.value && matrix[a][d] == material2.value && matrix[a][b] == material3.value && matrix[c][d] == material4.value && 
					material1.id[1] == a && material1.id[2] == e && material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == d && 
					material2.id[1]||material3.id[1] == a && material2.id[2]||material3.id[2] == b 
					&& material4.id[1] == c && material4.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Abbey";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[a][d] == material2.value && matrix[c][d] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] == e && material1.id[2] == d && material2.id[1] == a && material2.id[2] == d && material3.id[1] == c && material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[e][b] == material4.value &&
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == c && material2.id[2] == d && material3.id[1] == e && material3.id[2] == d 
					&& material4.id[1] == e && material4.id[2] == b)||
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[e][d] == material4.value &&
					material1.id[1] == a && material1.id[2] == b && material2.id[1] == a && material2.id[2] == d && material3.id[1] == e && material3.id[2] == b 
					&& material4.id[1] == e && material4.id[2] == d)||
					(matrix[e][b] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value && matrix[a][d] == material4.value &&
					material1.id[1] == e && material1.id[2] == b && material2.id[1] == a && material2.id[2] == b && material3.id[1] == c && material3.id[2] == b 
					&& material4.id[1] == a && material4.id[2] == d	)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Abbey";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}

function BuscarChapel(){
	if(chapel == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[c][e] == "Piedra" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Piedra" && matrix[a][b] == "Vidrio" && matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio")||
				(matrix[c][e] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[c][d] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio" && matrix[a][e] == "Piedra" && matrix[c][e] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
				//console.log("no");

			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][b] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[a][b] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio" && matrix[e][d] == "Piedra" && matrix[e][b] == "Vidrio")||
				(matrix[e][d] == "Piedra" && matrix[c][d] == "Vidrio" && matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio")||
				(matrix[a][b] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[e][b] == "Piedra" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log("no");
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEN=cantidad;
}
}
}
function Chapel(){
	if(chapel == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra':if(material1==undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio':if(material2==undefined){
			material2 = document.getElementById(celda2);
		}
		else
		{
			material4 = document.getElementById(celda2);
		} 
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Piedra':if(material1==undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio':if(material2==undefined){
			material2 = document.getElementById(celda3);
		}
		else
		{
			material4 = document.getElementById(celda3);
		} 
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Piedra':if(material1==undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio':if(material2==undefined){
			material2 = document.getElementById(celda4);
		}
		else
		{
			material4 = document.getElementById(celda4);
		} 
		break;
		default: problema = true;
		break;
	}
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][e] == material4.value &&
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b 
					&& material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == a && material2.id[1] || material4.id[1] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b 
					&& material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material4.id[2] || material4.id[2] == d)||
					(matrix[c][e] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b 
					&& material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material4.id[1] || material4.id[1] == a && material4.id[2] || material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][e] == material4.value && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b 
					&& material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == e)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Chapel";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][b] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[e][b] == material4.value &&
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == d 
					&& material2.id[2] || material4.id[1] == e && material2.id[2] || material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[e][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == d 
					&& material2.id[1] || material4.id[1] == e && material2.id[2] || material4.id[2] == b)||
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d 
					&& material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b)||
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[e][d] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b && material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == b 
					&& material2.id[1] || material4.id[1] == e && material2.id[2] || material4.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Chapel";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}

function BuscarCloister(){
	if(cloister == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Madera" && matrix[c][b] == "Ladrillo" && matrix[c][e] == "Piedra" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Madera" && matrix[a][b] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio")||
				(matrix[c][e] == "Madera" && matrix[c][b] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Madera" && matrix[a][b] == "Ladrillo" && matrix[a][e] == "Piedra" && matrix[c][e] == "Vidrio")
			){
				
				cantidad++;
				
			}
			else
			{
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][b] == "Madera" && matrix[c][b] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Madera" && matrix[c][d] == "Ladrillo" && matrix[e][d] == "Piedra" && matrix[e][b] == "Vidrio")||
				(matrix[e][d] == "Madera" && matrix[c][d] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio")||
				(matrix[a][b] == "Madera" && matrix[c][b] == "Ladrillo" && matrix[e][b] == "Piedra" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
			}
			else
			{
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEN=cantidad;
}
	}
}
function Cloister(){
	if(cloister == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Madera':material1 = document.getElementById(celda1);
		break;
		case 'Ladrillo': material2 = document.getElementById(celda1);
		break;
		case 'Piedra': material3 = document.getElementById(celda1);
		break;
		case 'Vidrio': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Madera':material1 = document.getElementById(celda2);
		break;
		case 'Ladrillo': material2 = document.getElementById(celda2);
		break;
		case 'Piedra': material3 = document.getElementById(celda2);
		break;
		case 'Vidrio': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Madera':material1 = document.getElementById(celda3);
		break;
		case 'Ladrillo': material2 = document.getElementById(celda3);
		break;
		case 'Piedra': material3 = document.getElementById(celda3);
		break;
		case 'Vidrio': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Madera':material1 = document.getElementById(celda4);
		break;
		case 'Ladrillo': material2 = document.getElementById(celda4);
		break;
		case 'Piedra': material3 = document.getElementById(celda4);
		break;
		case 'Vidrio': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][e] == material4.value &&
					material1.id[1] == c && material1.id[2] == d && material2.id[1] == c && material2.id[2] == b && material3.id[1] == c && material3.id[2] == e 
					&& material4.id[1] == a && material4.id[2] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value && 
					material1.id[1] == a && material1.id[2] == e && material2.id[1] == a && material2.id[2] == b && material3.id[1] == a && material3.id[2] == d 
					&& material4.id[1] == c && material4.id[2] == d)||
					(matrix[c][e] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value && 
					material1.id[1] == c && material1.id[2] == e && material2.id[1] == c && material2.id[2] == b && material3.id[1] == c && material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][e] == material4.value && 
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == a && material2.id[2] == b && material3.id[1] == a && material3.id[2] == e 
					&& material4.id[1] == c && material4.id[2] == e)				
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Cloister";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][b] == material1.value && matrix[c][b] == material2.value && matrix[a][b] == material3.value && matrix[a][d] == material4.value &&
					material1.id[1] == e && material1.id[2] == b && material2.id[1] == c && material2.id[2] == b && material3.id[1] == a && material3.id[2] == b 
					&& material4.id[1] == a && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[e][b] == material4.value &&
					material1.id[1] == a && material1.id[2] == d && material2.id[1] == c && material2.id[2] == d && material3.id[1] == e && material3.id[2] == d 
					&& material4.id[1] == e && material4.id[2] == b)||
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] == e && material1.id[2] == d && material2.id[1] == c && material2.id[2] == d && material3.id[1] == a && material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[e][d] == material4.value &&
					material1.id[1] == a && material1.id[2] == b && material2.id[1] == c && material2.id[2] == b && material3.id[1] == e && material3.id[2] == b 
					&& material4.id[1] == e && material4.id[2] == d	)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Cloister";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}

}

function BuscarTemple(){
	if(temple == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Ladrillo" && matrix[c][b] == "Ladrillo" && matrix[c][e] == "Piedra" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Ladrillo" && matrix[a][b] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio")||
				(matrix[c][e] == "Ladrillo" && matrix[c][b] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Ladrillo" && matrix[a][e] == "Piedra" && matrix[c][e] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log("No");
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][b] == "Ladrillo" && matrix[c][b] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Ladrillo" && matrix[e][d] == "Piedra" && matrix[e][b] == "Vidrio")||
				(matrix[e][d] == "Ladrillo" && matrix[c][d] == "Ladrillo" && matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio")||
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Ladrillo" && matrix[e][b] == "Piedra" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEN=cantidad;
}
	}
}
function Temple(){
	if(temple == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Ladrillo': material1 = document.getElementById(celda1);
		break;
		case 'Piedra': material3 = document.getElementById(celda1);
		break;
		case 'Vidrio': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Piedra': material3 = document.getElementById(celda2);
		break;
		case 'Vidrio': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Piedra': material3 = document.getElementById(celda3);
		break;
		case 'Vidrio': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material2 = document.getElementById(celda4);
		}
		break;
		case 'Piedra': material3 = document.getElementById(celda4);
		break;
		case 'Vidrio': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][e] == material4.value &&
					material1.id[1] || material2.id[1] == c && material1.id[2] || material2.id[2] == d && material1.id[1] || material2.id[1] == c && material1.id[2] || material2.id[2] == b && material3.id[1] == c && material3.id[2] == e 
					&& material4.id[1] == a && material4.id[2] == e)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][e] == material4.value && 
					material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == d && material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == b && material3.id[1] == a && material3.id[2] == e 
					&& material4.id[1] == c && material4.id[2] == e)||
					(matrix[c][e] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value && 
					material1.id[1] || material2.id[1] == c && material1.id[2] || material2.id[2] == e && material1.id[1] || material2.id[1] == c && material1.id[1] || material2.id[1] == b && material3.id[1] == c && material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == d)||
					(matrix[a][e] == material1.value && matrix[a][d] == material2.value && matrix[a][b] == material3.value && matrix[c][d] == material4.value && 
					material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == e && material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == d && material3.id[1] == a && material3.id[2] == b 
					&& material4.id[1] == c && material4.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tample";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[a][d] == material2.value && matrix[c][d] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material2.id[1] == e && material1.id[2] || material2.id[2] == d && material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == d && material3.id[1] == c && material3.id[2] == d 
					&& material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[e][b] == material4.value &&
					material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == d && material1.id[1] || material2.id[1] == c && material1.id[2] || material2.id[2] == d && material3.id[1] == e && material3.id[2] == d 
					&& material4.id[1] == e && material4.id[2] == b)||
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[e][d] == material4.value &&
					material1.id[1] || material2.id[1] == a && material1.id[2] || material2.id[2] == b && material1.id[1] || material2.id[1] == a && material1.id[1] || material2.id[1] == d && material3.id[1] == e && material3.id[2] == b 
					&& material4.id[1] == e && material4.id[2] == d)||
					(matrix[e][b] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value && matrix[a][d] == material4.value &&
					material1.id[1] || material2.id[1] == e && material1.id[2] || material2.id[2] == b && material1.id[1] || material2.id[1] == a && material1.id[1] || material2.id[1] == b && material3.id[1] == c && material3.id[2] == b 
					&& material4.id[1] == a && material4.id[2] == d	)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tample";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}

}

//Edifios verdes
function BuscarAlmshouse(){
	if(almshouse == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-1; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Piedra" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Piedra" && matrix[a][b] == "Piedra" && matrix[a][d] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 0;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-1; y++){
			if(
				(matrix[e][d] == "Piedra" && matrix[c][d] == "Piedra" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Piedra" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(d);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 0;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEV=cantidad;
}
	}
}
function Almshouse(){
	if(almshouse == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined)
	{
	var material1;
	var material2;
	var material3;
	var problema = false;
	contador2 = 0;
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	if(problema == false && material1 != undefined && material2 != undefined && material3 != undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == e && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Almshouse";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == e && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == a && material3.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == e && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Almshouse";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}
	}

}

function BuscarFeastHall(){
	if(feastHall == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-1; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[a][d] == "Madera" && matrix[a][b] == "Madera" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Madera" && matrix[a][b] == "Madera" && matrix[a][d] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 0;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-1; y++){
			if(
				(matrix[e][d] == "Madera" && matrix[c][d] == "Madera" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Madera" && matrix[c][d] == "Madera" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(d);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 0;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEV=cantidad;
}
	}
}
function FeastHall(){
	if(feastHall == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var material1;
	var material2;
	var material3;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined ){
	switch (materialFantasma1.value) {
		case 'Madera': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Madera': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Madera': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined ){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == e && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="FeastHall";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == e && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == a && material3.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == e && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="FeastHall";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{		
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}
}

function BuscarTavern(){
	if(tavern == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-1; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Ladrillo" && matrix[a][e] == "Vidrio")||
				(matrix[a][e] == "Ladrillo" && matrix[a][b] == "Ladrillo" && matrix[a][d] == "Vidrio")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 0;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-1; y++){
			if(
				(matrix[e][d] == "Ladrillo" && matrix[c][d] == "Ladrillo" && matrix[a][d] == "Vidrio")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Ladrillo" && matrix[e][d] == "Vidrio")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(d);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 0;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEV=cantidad;
}
	}
}
function Tavern(){
	if(tavern == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var material1;
	var material2;
	var material3;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined){
	switch (materialFantasma1.value) {
		case 'Ladrillo': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material3 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == e && 
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
					material3.id[1] == a && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tavern";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value &&
					material1.id[1]||material2.id[1] == e && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == a && material3.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value &&
					material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && 
					material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
					material3.id[1] == e && material3.id[2] == d)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tavern";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
		//celda1= undefined;
		//celda2= undefined;
		//celda3= undefined;
	}
}

}

//Edifios amarillos EY
function BuscarBakery(){
	if(bakery == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Ladrillo" && matrix[c][b] == "Vidrio" && matrix[c][e] == "Ladrillo" && matrix[a][b] == "Trigo")||
				(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Vidrio" && matrix[a][e] == "Ladrillo" && matrix[c][b] == "Trigo")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(c);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Vidrio" && matrix[e][b] == "Ladrillo" && matrix[c][d] == "Trigo")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Vidrio" && matrix[e][d] == "Ladrillo" && matrix[c][b] == "Trigo")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(a+" "+b+" = "+matrix[a][b]);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEY=cantidad;
}
	}
}
function Bakery(){
	if(bakery == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Ladrillo': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Trigo': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Trigo': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}

	switch (materialFantasma3.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio ': material2 = document.getElementById(celda3);
		break;
		case 'Trigo': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Ladrillo': if(material1== undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda4);
		break;
		case 'Trigo': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == a && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Bakery";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == b && material4.id[1] == c && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == d && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Bakery";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}
}

function BuscarMarket(){
	if(market == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[c][e] == "Piedra" && matrix[a][b] == "Madera")||
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio" && matrix[a][e] == "Piedra" && matrix[c][b] == "Madera")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(c);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[e][b] == "Piedra" && matrix[c][d] == "Madera")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio" && matrix[e][d] == "Piedra" && matrix[c][b] == "Madera")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(a+" "+b+" = "+matrix[a][b]);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEY=cantidad;
}
	}
}
function Market(){
	if(market == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Madera': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Madera': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}

	switch (materialFantasma3.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda3);
		break;
		case 'Madera': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda4);
		break;
		case 'Madera': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema=true;
	}

	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == a && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Market";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == b && material4.id[1] == c && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == d && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Market";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}

}

function BuscarTailor(){
	if(tailor == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[c][e] == "Piedra" && matrix[a][b] == "Trigo")||
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Vidrio" && matrix[a][e] == "Piedra" && matrix[c][b] == "Trigo")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(c);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Piedra" && matrix[c][b] == "Vidrio" && matrix[e][b] == "Piedra" && matrix[c][d] == "Trigo")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Vidrio" && matrix[e][d] == "Piedra" && matrix[c][b] == "Trigo")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(a+" "+b+" = "+matrix[a][b]);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEY=cantidad;
}
	}
}
function Tailor(){
	if(tailor == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Trigo': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Trigo': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}

	switch (materialFantasma3.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda3);
		break;
		case 'Trigo': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Piedra': if(material1== undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda4);
		break;
		case 'Trigo': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == a && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tailor";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == b && material4.id[1] == c && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == d && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Tailor";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
	}
}

function BuscarTheater(){
	if(theater == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[c][d] == "Madera" && matrix[c][b] == "Vidrio" && matrix[c][e] == "Madera" && matrix[a][b] == "Piedra")||
				(matrix[a][d] == "Madera" && matrix[a][b] == "Vidrio" && matrix[a][e] == "Madera" && matrix[c][b] == "Piedra")
			){
				
				cantidad++;
				console.log("SI");
				
			}
			else
			{
              //console.log(c);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Madera" && matrix[c][b] == "Vidrio" && matrix[e][b] == "Madera" && matrix[c][d] == "Piedra")||
				(matrix[a][d] == "Madera" && matrix[c][d] == "Vidrio" && matrix[e][d] == "Madera" && matrix[c][b] == "Piedra")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(a+" "+b+" = "+matrix[a][b]);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEY=cantidad;
}
} 
}
function Theater(){
	if(theater == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Madera': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Piedra': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Madera': if(material1== undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Piedra': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}

	switch (materialFantasma3.value) {
		case 'Madera': if(material1== undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda3);
		break;
		case 'Piedra': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Madera': if(material1== undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Vidrio': material2 = document.getElementById(celda4);
		break;
		case 'Piedra': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema= true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][b] == material4.value &&
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material4.id[1] == a && material4.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == a && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Theater";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[c][d] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] == c && material2.id[2] == b && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == b && material4.id[1] == c && material4.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[c][b] == material4.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] == c && material2.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[1] || material3.id[1] == d && material4.id[1] == c && material4.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Theater";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
		celda1= undefined;
		celda2= undefined;
		celda3= undefined;
	}
}

}

//Edificios Azules
function BuscarTradingPost(){
	if(tradingPost == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Madera" && matrix[c][d] == "Piedra" && matrix[c][b] == "Madera" && matrix[c][e] == "Ladrillo")||
				(matrix[a][e] == "Piedra" && matrix[a][b] == "Madera" && matrix[c][e] == "Piedra" && matrix[c][b] == "Madera" && matrix[a][d] == "Ladrillo")||
				(matrix[a][e] == "Piedra" && matrix[a][b] == "Madera" && matrix[c][e] == "Piedra" && matrix[c][b] == "Madera" && matrix[c][d] == "Ladrillo")||
				(matrix[a][d] == "Piedra" && matrix[a][b] == "Madera" && matrix[c][d] == "Piedra" && matrix[c][b] == "Madera" && matrix[a][e] == "Ladrillo")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              //console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][d] == "Piedra" && matrix[c][d] == "Madera" && matrix[e][b] == "Piedra" && matrix[c][b] == "Madera" && matrix[a][b] == "Ladrillo")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Madera" && matrix[a][b] == "Piedra" && matrix[c][b] == "Madera" && matrix[e][d] == "Ladrillo")||
				(matrix[e][d] == "Piedra" && matrix[c][d] == "Madera" && matrix[e][b] == "Piedra" && matrix[c][b] == "Madera" && matrix[a][d] == "Ladrillo")||
				(matrix[a][d] == "Piedra" && matrix[c][d] == "Madera" && matrix[a][b] == "Piedra" && matrix[c][b] == "Madera" && matrix[e][b] == "Ladrillo")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
			//	console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEA=cantidad;
}
	}
}
function TradingPost(){
	if(tradingPost == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var materialFantasma5 = document.getElementById(celda5);
	var material1;
	var material2;
	var material3;
	var material4;
	var material5;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined && materialFantasma5 !=undefined){
	switch (materialFantasma1.value) {
		case 'Piedra': material1 = document.getElementById(celda1);
		break;
		case 'Madera': material2 = document.getElementById(celda1);
		break;
		case 'Ladrillo': material5 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Piedra': if(material1 == undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Madera': if(material2 == undefined){
			material2 = document.getElementById(celda2);
		}
		else
		{
			material4 = document.getElementById(celda2);
		}
		break;
		case 'Ladrillo': material5 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Piedra': if(material1 == undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Madera': if(material2 == undefined){
			material2 = document.getElementById(celda3);
		}
		else
		{
			material4 = document.getElementById(celda3);
		}
		break;
		case 'Ladrillo': material5 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Piedra': if(material1 == undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Madera': if(material2 == undefined){
			material2 = document.getElementById(celda4);
		}
		else
		{
			material4 = document.getElementById(celda4);
		}
		break;
		case 'Ladrillo': material5 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma5.value) {
		case 'Piedra': if(material1 == undefined){
			material1 = document.getElementById(celda5);
		}
		else
		{
			material3 = document.getElementById(celda5);
		}
		break;
		case 'Madera': if(material2 == undefined){
			material2 = document.getElementById(celda5);
		}
		else
		{
			material4 = document.getElementById(celda5);
		}
		break;
		case 'Ladrillo': material5 = document.getElementById(celda5);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema =true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined && material5 != undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value && matrix[c][e] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == c && material5.id[2] == e)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[c][e] == material3.value && matrix[c][b] == material4.value && matrix[a][d] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == a && material5.id[2] == d)||
					(matrix[a][e] == material1.value && matrix[a][b] == material2.value && matrix[c][e] == material3.value && matrix[c][b] == material4.value && matrix[c][d] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == e && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == c && material5.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value && matrix[a][e] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == a && material2.id[2] || material4.id[2] == b && 
					material1.id[1] || material3.id[1] == c && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == a && material5.id[2] == e)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	matrix[material5.id[1]][material5.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	material5.innerHTML="";
	material5.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Theater";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
	material5.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[e][b] == material3.value && matrix[c][b] == material4.value && matrix[a][b] == material5.value &&
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == a && material5.id[2] == b)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value && matrix[e][d] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == e && material5.id[2] == d)||
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[e][b] == material3.value && matrix[c][b] == material4.value && matrix[a][d] == material5.value &&
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && 
					material1.id[1] || material3.id[1] == e && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == a && material5.id[2] == d)||
					(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value && matrix[e][b] == material5.value &&
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == d && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == d && 
					material1.id[1] || material3.id[1] == a && material1.id[2] || material3.id[2] == b && material2.id[1] || material4.id[1] == c && material2.id[2] || material4.id[2] == b &&
					material5.id[1] == e && material5.id[2] == b)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	material5.innerHTML="";
	material5.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Theater";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
	material5.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
			}
			else
			{
				b++;
				d++;
				f++;
				marcador++;
			}
		}
		a++;
		c++;
		e++;
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}

}

function BuscarWarehouse(){
	if(warehouse == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-3; y++){
			if(
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Madera" && matrix[a][e] == "Trigo" && matrix[c][d] == "Ladrillo" && matrix[c][e] == "Ladrillo")||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Madera" && matrix[c][e] == "Trigo" && matrix[a][d] == "Ladrillo" && matrix[a][e] == "Ladrillo")
			){
				
				cantidad++;
				//console.log("SI");
				
			}
			else
			{
              ///console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			e = 2;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			e++;
			marcador++;
		}
	}
	a++;
	c++;
}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	var f = 2; //dentro
	marcador = 1;
	for(var x = 0; x <= matrix.length-3; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[e][d] == "Trigo" && matrix[c][d] == "Madera" && matrix[a][d] == "Trigo" && matrix[e][b] == "Ladrillo" && matrix[a][b] == "Ladrillo")||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Madera" && matrix[e][b] == "Trigo" && matrix[a][d] == "Ladrillo" && matrix[e][d] == "Ladrillo")
			){
				cantidad++;
				//console.log("Si");
			}
			else
			{
				//console.log(a);
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			f = 2;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			f++;
			marcador++;
		}
	}
	a++;
	c++;
	e++;
}
if (estado == false){
	totalEA=cantidad;
}
	}
}
function Warehouse(){
	if(warehouse == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var materialFantasma5 = document.getElementById(celda5);
	var material1;
	var material2;
	var material3;
	var material4;
	var material5;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined && materialFantasma5 !=undefined){
	switch (materialFantasma1.value) {
		case 'Trigo': material1 = document.getElementById(celda1);
		break;
		case 'Madera': material2 = document.getElementById(celda1);
		break;
		case 'Ladrillo': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Madera': material2 = document.getElementById(celda2);
		break;
		case 'Ladrillo': if(material4 == undefined) {
			material4 = document.getElementById(celda2);
		} 
		else
		{
			material5 = document.getElementById(celda2);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Madera': material2 = document.getElementById(celda3);
		break;
		case 'Ladrillo': if(material4 == undefined) {
			material4 = document.getElementById(celda3);
		} 
		else
		{
			material5 = document.getElementById(celda3);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Madera': material2 = document.getElementById(celda4);
		break;
		case 'Ladrillo': if(material4 == undefined) {
			material4 = document.getElementById(celda4);
		} 
		else
		{
			material5 = document.getElementById(celda4);
		}
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma5.value) {
		case 'Trigo': if(material1 == undefined){
			material1 = document.getElementById(celda5);
		}
		else
		{
			material3 = document.getElementById(celda5);
		}
		break;
		case 'Madera': material2 = document.getElementById(celda5);
		break;
		case 'Ladrillo': if(material4 == undefined) {
			material4 = document.getElementById(celda5);
		} 
		else
		{
			material5 = document.getElementById(celda5);
		}
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined && material5 != undefined){
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//afuera
		var marcador = 2;
		for(var x = 0; x <= matrix.length-2; x++){
			for(var y = 0; y <= matrix.length-3; y++){
				if(
					(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[c][d] == material4.value && matrix[c][e] == material5.value &&
					material1.id[1]||material3.id[1]== a && material1.id[2]||material3.id[2]== d && material2.id[1]== a && material2.id[2]== b && 
					material1.id[1]||material3.id[1]== a && material1.id[2]||material3.id[2]== e && material4.id[1]||material5.id[1]== c && 
					material4.id[2]||material5.id[2]== d && material4.id[1]||material5.id[1]== c && material4.id[2]||material5.id[2]== e)||
					(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[a][d] == material4.value && matrix[a][e] == material5.value &&
					material1.id[1]||material3.id[1]== c && material1.id[2]||material3.id[2]== d && material2.id[1]== c && material2.id[2]== b && 
					material1.id[1]||material3.id[1]==c && material1.id[2]||material3.id[2]== e && material4.id[1]||material5.id[1]== a && 
					material4.id[2]||material5.id[2]== d && material4.id[1]||material5.id[1]== a && material4.id[2]||material5.id[2]== e)
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	matrix[material5.id[1]][material5.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	material5.innerHTML="";
	material5.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Warehouse";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
	material5.addEventListener("click",Construir);
				}
				else
				{
	
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				e = 2;
				marcador = 2;
			}
			else
			{
				b++;
				d++;
				e++;
				marcador++;
			}
		}
		a++;
		c++;
	}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		var f = 2; //dentro
		marcador = 1;
		for(var x = 0; x <= matrix.length-3; x++){
			for(var y = 0; y <= matrix.length-2; y++){
				if(
					(matrix[e][d] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value && matrix[e][b] == material4.value && matrix[a][b] == material5.value &&
					material1.id[1]||material3.id[1]== e && material1.id[2]||material3.id[2]== d && material2.id[1]== c && material2.id[2]== d && 
					material1.id[1]||material3.id[1]== a && material1.id[2]||material3.id[2]== d && material4.id[1]||material5.id[1]== e && 
					material4.id[2]||material5.id[2]== b && material4.id[1]||material5.id[1]== a && material4.id[2]||material5.id[2]== b)||
					(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[a][d] == material4.value && matrix[e][d] == material5.value &&
					material1.id[1]||material3.id[1]== a && material1.id[2]||material3.id[2]== b && material2.id[1]== c && material2.id[2]== b && 
					material1.id[1]||material3.id[1]== e && material1.id[2]||material3.id[2]== b && material4.id[1]||material5.id[1]== a && 
					material4.id[2]||material5.id[2]== d && material4.id[1]||material5.id[1]== e && material4.id[2]||material5.id[2]== d) 
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
	matrix[material2.id[1]][material2.id[2]]="undefined";
	matrix[material3.id[1]][material3.id[2]]="undefined";
	matrix[material4.id[1]][material4.id[2]]="undefined";
	material1.innerHTML="";
	material1.value = undefined;
	material2.innerHTML="";
	material2.value = undefined;
	material3.innerHTML="";
	material3.value = undefined;
	material4.innerHTML="";
	material4.value = undefined;
	material5.innerHTML="";
	material5.value = undefined;
	var i = 0;
	while(i < tabla.length){
		tabla[i].removeEventListener("click",TMC);
		i++;
	}
	construccion="Warehouse";
	material1.addEventListener("click",Construir);
	material2.addEventListener("click",Construir);
	material3.addEventListener("click",Construir); 
	material4.addEventListener("click",Construir);
	material5.addEventListener("click",Construir);
				}
				else
				{
				}
				if(marcador == 3){
				d = 0;
				b = 1;
				f = 2;
				marcador = 1;
				}
					else
					{
						b++;
						d++;
						f++;
						marcador++;
					}
				}
				a++;
				c++;
				e++;
			}                        	
		}	
		else
		{

			var botonA = document.getElementById("botonA");
			botonA.disabled=false;
		}
	}
}

function BuscarFactory(){
	if(factory == true){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var e = 2;//dentro
	var f = 3;//dentro
	for(var x = 0; x <= matrix.length-2; x++){
		if(
			(matrix[c][d] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[c][e] == "Piedra" && matrix[c][f] == "Ladrillo" && matrix[a][d] == "Madera")||
			(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[a][e] == "Piedra" && matrix[a][f] == "Ladrillo" && matrix[c][f] == "Madera")||
			(matrix[c][d] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[c][e] == "Piedra" && matrix[c][f] == "Ladrillo" && matrix[a][f] == "Madera")||
			(matrix[a][d] == "Ladrillo" && matrix[a][b] == "Piedra" && matrix[a][e] == "Piedra" && matrix[a][f] == "Ladrillo" && matrix[c][d] == "Madera")
			){
				
				cantidad++;
			//	console.log("SI");
				
			}
			else
			{
              //console.log(a);
			}
		a++;
		c++;
	}
	a = 0;//afuera
	b = 1;//dentro
	c = 1;//afuera
	d = 0;//dentro
	e = 2;//afuera
	f = 2; //dentro
	 var g = 3 //afuera
	marcador = 1;
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[e][b] == "Piedra" && matrix[g][b] == "Ladrillo" && matrix[g][d] == "Madera")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[e][d] == "Piedra" && matrix[g][d] == "Ladrillo" && matrix[a][b] == "Madera")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[e][d] == "Piedra" && matrix[g][d] == "Ladrillo" && matrix[g][b] == "Madera")||
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[e][b] == "Piedra" && matrix[g][b] == "Ladrillo" && matrix[a][d] == "Madera")
			){
				cantidad++;
			//	console.log("Si");
			}
			else
			{
				//console.log(f);
			}
			b++;
			d++;
			f++;
		marcador++;
		
	}
if (estado == false){
	totalEA=cantidad;
}
	}
}
function Factory(){
	if(factory == true){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var materialFantasma5 = document.getElementById(celda5);
	var material1;
	var material2;
	var material3;
	var material4;
	var material5;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined && materialFantasma5 !=undefined){
	switch (materialFantasma1.value) {
		case 'Ladrillo': material1 = document.getElementById(celda1);
		break;
		case 'Piedra': material2 = document.getElementById(celda1);
		break;
		case 'Madera': material5 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Ladrillo': if(material1 == undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material4 = document.getElementById(celda2);
		}
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda2);
		}
		else
		{
			material3 = document.getElementById(celda2);
		}
		break;
		case 'Madera': material5 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Ladrillo': if(material1 == undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material4 = document.getElementById(celda3);
		}
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda3);
		}
		else
		{
			material3 = document.getElementById(celda3);
		}
		break;
		case 'Madera': material5 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Ladrillo': if(material1 == undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material4 = document.getElementById(celda4);
		}
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda4);
		}
		else
		{
			material3 = document.getElementById(celda4);
		}
		break;
		case 'Madera': material5 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma5.value) {
		case 'Ladrillo': if(material1 == undefined){
			material1 = document.getElementById(celda5);
		}
		else
		{
			material4 = document.getElementById(celda5);
		}
		break;
		case 'Piedra': if(material2 == undefined){
			material2 = document.getElementById(celda5);
		}
		else
		{
			material3 = document.getElementById(celda5);
		}
		break;
		case 'Madera': material5 = document.getElementById(celda5);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined && material5 != undefined){
		var cantidad=0;	
		var a = 0;//afuera
		var b = 1;//dentro
		var c = 1;//afuera
		var d = 0;//dentro
		var e = 2;//dentro
		var f = 3;//dentro
		for(var x = 0; x <= matrix.length-2; x++){
			if(
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[c][f] == material4.value && matrix[a][d] == material5.value &&
				material1.id[1]||material4.id[1]== c && material1.id[2]||material4.id[2]== d &&
				material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== b &&
				material2.id[1]||material3.id[1]== c && material2.id[1]||material3.id[1]== e &&
				material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== d &&
				material5.id[1]== a && material5.id[2]== b )||
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[a][f] == material4.value && matrix[c][f] == material5.value &&
				material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== d &&
				material2.id[1]||material3.id[1]== a && material2.id[2]||material3.id[2]== b &&
				material2.id[1]||material3.id[1]== a && material2.id[1]||material3.id[1]== e &&
				material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== f &&
				material5.id[1]== c && material5.id[2]== f )||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[c][e] == material3.value && matrix[c][f] == material4.value && matrix[a][f] == material5.value &&
				material1.id[1]||material4.id[1]== c && material1.id[2]||material4.id[2]== d &&
				material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== b &&
				material2.id[1]||material3.id[1]== c && material2.id[1]||material3.id[1]== e &&
				material1.id[1]||material4.id[1]== c && material1.id[2]||material4.id[2]== f &&
				material5.id[1]== a && material5.id[2]== f )||
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[a][e] == material3.value && matrix[a][f] == material4.value && matrix[c][d] == material5.value &&
				material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== d &&
				material2.id[1]||material3.id[1]== a && material2.id[2]||material3.id[2]== b &&
				material2.id[1]||material3.id[1]== a && material2.id[1]||material3.id[1]== e &&
				material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== f &&
				material5.id[1]== c && material5.id[2]== d )
				){
					matrix[material1.id[1]][material1.id[2]]="undefined";
					matrix[material2.id[1]][material2.id[2]]="undefined";
					matrix[material3.id[1]][material3.id[2]]="undefined";
					matrix[material4.id[1]][material4.id[2]]="undefined";
					matrix[material5.id[1]][material5.id[2]]="undefined";
					material1.innerHTML="";
					material1.value = undefined;
					material2.innerHTML="";
					material2.value = undefined;
					material3.innerHTML="";
					material3.value = undefined;
					material4.innerHTML="";
					material4.value = undefined;
					material5.innerHTML="";
					material5.value = undefined;
					var i = 0;
					while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
					}
					construccion="Factory";
					material1.addEventListener("click",Construir);
					material2.addEventListener("click",Construir);
					material3.addEventListener("click",Construir); 
					material4.addEventListener("click",Construir);
					material5.addEventListener("click",Construir);
					
				}
				else
				{

				}
			a++;
			c++;
		}
		a = 0;//afuera
		b = 1;//dentro
		c = 1;//afuera
		d = 0;//dentro
		e = 2;//afuera
		f = 2; //dentro
		var g = 3 //afuera
		marcador = 1;
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				/*(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[e][b] == "Piedra" && matrix[g][b] == "Ladrillo" && matrix[g][d] == "Madera")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[e][d] == "Piedra" && matrix[g][d] == "Ladrillo" && matrix[a][b] == "Madera")||
				(matrix[a][d] == "Ladrillo" && matrix[c][d] == "Piedra" && matrix[e][d] == "Piedra" && matrix[g][d] == "Ladrillo" && matrix[g][b] == "Madera")||
				(matrix[a][b] == "Ladrillo" && matrix[c][b] == "Piedra" && matrix[e][b] == "Piedra" && matrix[g][b] == "Ladrillo" && matrix[a][d] == "Madera")*/
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[g][b] == material4.value && matrix[g][d] == material5.value &&
					material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== b &&
					material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== b &&
					material2.id[1]||material3.id[1]== e && material2.id[1]||material3.id[1]== b &&
					material1.id[1]||material4.id[1]== g && material1.id[2]||material4.id[2]== b &&
					material5.id[1]== g && material5.id[2]== d )||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[g][d] == material4.value && matrix[a][b] == material5.value &&
					material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== d &&
					material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== d &&
					material2.id[1]||material3.id[1]== e && material2.id[1]||material3.id[1]== d &&
					material1.id[1]||material4.id[1]== g && material1.id[2]||material4.id[2]== d &&
					material5.id[1]== a && material5.id[2]== b )||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[e][d] == material3.value && matrix[g][d] == material4.value && matrix[g][b] == material5.value &&
					material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== d &&
					material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== d &&
					material2.id[1]||material3.id[1]== e && material2.id[1]||material3.id[1]== d &&
					material1.id[1]||material4.id[1]== g && material1.id[2]||material4.id[2]== d &&
					material5.id[1]== g && material5.id[2]== b )||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[e][b] == material3.value && matrix[g][b] == material4.value && matrix[a][d] == material5.value &&
					material1.id[1]||material4.id[1]== a && material1.id[2]||material4.id[2]== b &&
					material2.id[1]||material3.id[1]== c && material2.id[2]||material3.id[2]== b &&
					material2.id[1]||material3.id[1]== e && material2.id[1]||material3.id[1]== b &&
					material1.id[1]||material4.id[1]== g && material1.id[2]||material4.id[2]== b &&
					material5.id[1]== a && material5.id[2]== d )
			){
					matrix[material1.id[1]][material1.id[2]]="undefined";
					matrix[material2.id[1]][material2.id[2]]="undefined";
					matrix[material3.id[1]][material3.id[2]]="undefined";
					matrix[material4.id[1]][material4.id[2]]="undefined";
					matrix[material5.id[1]][material5.id[2]]="undefined";
					material1.innerHTML="";
					material1.value = undefined;
					material2.innerHTML="";
					material2.value = undefined;
					material3.innerHTML="";
					material3.value = undefined;
					material4.innerHTML="";
					material4.value = undefined;
					material5.innerHTML="";
					material5.value = undefined;
					var i = 0;
					while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					i++;
					}
					construccion="Factory";
					material1.addEventListener("click",Construir);
					material2.addEventListener("click",Construir);
					material3.addEventListener("click",Construir); 
					material4.addEventListener("click",Construir);
					material5.addEventListener("click",Construir);
			}
			else
			{
			}
			b++;
			d++;
			f++;
		marcador++;
		
	}                        	
	}
	else
	{

		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}

}
///Monumentos
 // CathedralOfCaterina
function BuscarCOC(){
	if (coc == true && monumento == 0){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[c][d] == "Piedra")||
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[c][b] == "Piedra")||
				(matrix[c][d] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[a][b] == "Piedra")||
				(matrix[c][b] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[a][d] == "Piedra")||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Vidrio" && matrix[c][b] == "Piedra")||
				(matrix[a][b] == "Trigo" && matrix[a][d] == "Vidrio" && matrix[c][d] == "Piedra")||
				(matrix[c][b] == "Trigo" && matrix[a][b] == "Vidrio" && matrix[a][d] == "Piedra")||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Vidrio" && matrix[a][b] == "Piedra")
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	//Total de casas
	totalM=cantidad; //TDC
}
}
}

function COC(){
	if (coc == true && monumento == 0){
	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var material1;
	var material2;
	var material3;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined){
	switch (materialFantasma1.value) {
		case 'Trigo': material1 = document.getElementById(celda1);
		break;
		case 'Vidrio': material2 = document.getElementById(celda1);
		break;
		case 'Piedra': material3 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': material1 = document.getElementById(celda2);
		break;
		case 'Vidrio': material2 = document.getElementById(celda2);
		break;
		case 'Piedra': material3 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': material1 = document.getElementById(celda3);
		break;
		case 'Vidrio': material2 = document.getElementById(celda3);
		break;
		case 'Piedra': material3 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined){
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value)||
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value)||
				(matrix[c][d] == material1.value && matrix[a][d] == material2.value && matrix[a][b] == material3.value)||
				(matrix[c][b] == material1.value && matrix[c][d] == material2.value && matrix[a][d] == material3.value)||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[c][b] == material3.value)||
				(matrix[a][b] == material1.value && matrix[a][d] == material2.value && matrix[c][d] == material3.value)||
				(matrix[c][b] == material1.value && matrix[a][b] == material2.value && matrix[a][d] == material3.value)||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][b] == material3.value)
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";;
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					//console.log(tabla[i]);
					i++;
				}
				construccion="CathedralOfCaterina";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir); 
				monumento =1;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}   	
	}
	else
	{
		var botonA = document.getElementById("botonA");
		botonA.disabled=true;
	}
}
}

//Archive Of The Second Age
function BuscarASA(){
	if(asa == true && monumento == 0){
	var cantidad=0;	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 1;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][d] == "Ladrillo" && matrix[c][b] == "Vidrio") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[c][b] == "Ladrillo" && matrix[a][b] == "Vidrio") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][b] == "Ladrillo" && matrix[a][d] == "Vidrio") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Ladrillo" && matrix[c][d] == "Vidrio") ||
				
				(matrix[a][d] == "Trigo" && matrix[a][b] == "Trigo" && matrix[c][b] == "Ladrillo" && matrix[c][d] == "Vidrio") ||
				(matrix[a][b] == "Trigo" && matrix[c][b] == "Trigo" && matrix[c][d] == "Ladrillo" && matrix[a][d] == "Vidrio") ||
				(matrix[c][d] == "Trigo" && matrix[c][b] == "Trigo" && matrix[a][d] == "Ladrillo" && matrix[a][b] == "Vidrio") ||
				(matrix[a][d] == "Trigo" && matrix[c][d] == "Trigo" && matrix[a][b] == "Ladrillo" && matrix[c][b] == "Vidrio") 
				
			){
				cantidad++;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 3){
			d = 0;
			b = 1;
			marcador = 1;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}
if (estado == false){
	totalM=cantidad;
}
}
}
function ASA(){
	if(asa == true && monumento == 0){

	var materialFantasma1 = document.getElementById(celda1);
	var materialFantasma2 = document.getElementById(celda2);
	var materialFantasma3 = document.getElementById(celda3);
	var materialFantasma4 = document.getElementById(celda4);
	var material1;
	var material2;
	var material3;
	var material4;
	var problema = false;
	contador2 = 0;
	if(materialFantasma1 != undefined && materialFantasma2 != undefined && materialFantasma3 != undefined && materialFantasma4 != undefined){
	switch (materialFantasma1.value) {
		case 'Trigo': material1 = document.getElementById(celda1);
		break;
		case 'Ladrillo': material3 = document.getElementById(celda1);
		break;
		case 'Vidrio': material4 = document.getElementById(celda1);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma2.value) {
		case 'Trigo': if(material1==undefined){
			material1 = document.getElementById(celda2);
		}
		else
		{
			material2 = document.getElementById(celda2);
		}
		break;
		case 'Ladrillo': material3 = document.getElementById(celda2);
		break;
		case 'Vidrio': material4 = document.getElementById(celda2);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma3.value) {
		case 'Trigo': if(material1==undefined){
			material1 = document.getElementById(celda3);
		}
		else
		{
			material2 = document.getElementById(celda3);
		}
		break;
		case 'Ladrillo': material3 = document.getElementById(celda3);
		break;
		case 'Vidrio': material4 = document.getElementById(celda3);
		break;
		default: problema = true;
		break;
	}
	switch (materialFantasma4.value) {
		case 'Trigo': if(material1==undefined){
			material1 = document.getElementById(celda4);
		}
		else
		{
			material2 = document.getElementById(celda4);
		}
		break;
		case 'Ladrillo': material3 = document.getElementById(celda4);
		break;
		case 'Vidrio': material4 = document.getElementById(celda4);
		break;
		default: problema = true;
		break;
	}
	}
	else
	{
		problema = true;
	}
	if(problema == false && material1 !=undefined && material2 !=undefined && material3 !=undefined && material4 !=undefined){	
	var a = 0;//afuera
	var b = 1;//dentro
	var c = 1;//afuera
	var d = 0;//dentro
	var marcador = 2;
	for(var x = 0; x <= matrix.length-2; x++){
		for(var y = 0; y <= matrix.length-2; y++){
			if(
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][d] == material3.value && matrix[c][b] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == c && material3.id[2] == d && material4.id[1] == c && material4.id[2] == b) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[c][b] == material3.value && matrix[a][b] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
				material3.id[1] == c && material3.id[2] == b && material4.id[1] == a && material4.id[2] == b) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][b] == material3.value && matrix[a][d] == material4.value &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == a && material3.id[2] == b && material4.id[1] == a && material4.id[2] == d) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[c][d] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == a && material3.id[2] == d && material4.id[1] == c && material4.id[2] == d) ||
				
				(matrix[a][d] == material1.value && matrix[a][b] == material2.value && matrix[c][b] == material3.value && matrix[c][d] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == c && material3.id[2] == b && material4.id[1] == c && material4.id[2] == d) ||
				(matrix[a][b] == material1.value && matrix[c][b] == material2.value && matrix[c][d] == material3.value && matrix[a][d] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == b && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == c && material3.id[2] == d && material4.id[1] == a && material4.id[2] == d) ||
				(matrix[c][d] == material1.value && matrix[c][b] == material2.value && matrix[a][d] == material3.value && matrix[a][b] == material4.value &&
				material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == b && 
				material3.id[1] == a && material3.id[2] == d && material4.id[1] == a && material4.id[2] == b) ||
				(matrix[a][d] == material1.value && matrix[c][d] == material2.value && matrix[a][b] == material3.value && matrix[c][b] == material4.value &&
				material1.id[1]||material2.id[1] == a && material1.id[2]||material2.id[2] == d && material1.id[1]||material2.id[1] == c && material1.id[2]||material2.id[2] == d && 
				material3.id[1] == a && material3.id[2] == b && material4.id[1] == c && material4.id[2] == b) 
			){
				matrix[material1.id[1]][material1.id[2]]="undefined";
				matrix[material2.id[1]][material2.id[2]]="undefined";
				matrix[material3.id[1]][material3.id[2]]="undefined";
				matrix[material4.id[1]][material4.id[2]]="undefined";
				material1.innerHTML="";
				material1.value = undefined;
				material2.innerHTML="";
				material2.value = undefined;
				material3.innerHTML="";
				material3.value = undefined;
				material4.innerHTML="";
				material4.value = undefined;
				var i = 0;
				while(i < tabla.length){
					tabla[i].removeEventListener("click",TMC);
					//console.log(tabla[i]);
					i++;
				}
				construccion="ASA";
				material1.addEventListener("click",Construir);
				material2.addEventListener("click",Construir);
				material3.addEventListener("click",Construir);
				material4.addEventListener("click",Construir); 
				monumento = 1;
			}
			else
			{
				//console.log("No se encontro");
			}
			if(marcador == 4){
			d = 0;
			b = 1;
			marcador = 2;
		}
		else
		{
			b++;
			d++;
			marcador++;
		}
	}
	a++;
	c++;
}   	
	}
	else
	{
		// no detecta orientación normal
		document.getElementById("MENSAJE").innerHTML = "No se puede construir."; 
		var botonA = document.getElementById("botonA");
		botonA.disabled=false;
	}
}

}


function Fin2(){
		

	var materiales_contador = 0;

	while(i < tabla.length){

		var mat_1 = tabla[i].value;

		switch (mat_1) {

		//materiales 
		case "Piedra":
		materiales_contador++;
		break;			
		case "Madera":
		materiales_contador++;
		break;			
		case "Trigo":
		materiales_contador++;
		break;			
		case "Vidrio":
		materiales_contador++;
		break;			
		case "Ladrillo":
		materiales_contador++;
		break;			
		}
		i++;
	}

	penalización = (-materiales_contador);

// contador edificios

edificio_contador = c_asa+c_coc+c_factory+c_tradingpost+c_warehouse+c_market+c_tailor+c_theater+c_bakery+c_almshouse+c_feasthall+c_tavern+c_cloister+c_tample+c_chappel+c_abbey+c_well+c_shed+c_millstone+c_fountain+c_orchard+c_greenhouse+c_granary+c_cottage+c_farm;

// calcular puntos crudos

			
		// código almshouse

		if (c_almshouse == 1)
		{
			c2_almshouse = -1;
		}
		else if (c_almshouse == 2)
		{
			c2_almshouse = 5;
		}
		else if (c_almshouse == 3)
		{
			c2_almshouse = -3;
		}
		else if (c_almshouse == 4)
		{
			c2_almshouse = 15;
		}
		else if (c_almshouse == 5)
		{
			c2_almshouse = -5;
		}
		else if (c_almshouse == 6)
		{
			c2_almshouse = 26;
		}

		//código tavern

		var c2_tavern = 0;
		
		if (c_tavern == 1)
		{
			c2_tavern = 2;
		}
		else if (c_tavern == 2)
		{
			c2_tavern = 5;
		}
		else if (c_tavern == 3)
		{
			c2_tavern = 9;
		}
		else if (c_tavern == 4)
		{
			c2_tavern = 14;
		}
		else if (c_tavern == 5)
		{
			c2_tavern = 20;
		}

		// console.log("<<-->> "+c_cottage);

var puntoscrudos = (c_cottage*3+c_cloister*1+c_chappel*1+c_factory*4+c_orchard*1+c_granary*1+c_greenhouse*1+c_fountain*1+c_millstone*1+c_shed*1+c_well*1+c_abbey*2+c_tample*2+c_feasthall*3+c_bakery*3+c_theater*3+c_tailor*4+c_market*4+c_warehouse*4+c_tradingpost*4+c_coc*8+c_asa*8);
					// no se ha añadido well (falta código )
					// no se ha añadido fountain (falta código )
					// no se ha añadido well (falta código )
					// no se ha añadido cottage (falta código )
					// no se ha añadido market (falta código )
					// no se ha añadido theater, tailor, bakery,  (falta código )
					// notaVolver

		total_puntos = puntoscrudos + penalización;
		
		// console.log("materiales total:"+materiales_contador);
		// console.log("__:"+materiales_contador);
		// console.log("<<-->> "+c_cottage);
		// console.log("<<-->> "+c_cloister);
		// console.log("<<-->> "+c_chappel);
		// console.log("<<-->> "+c_factory);
		// console.log("<<-->> "+c_orchard);
		// console.log("<<-->> "+c_granary);
		// console.log("<<-->> "+c_greenhouse);
		// console.log("<<-->> "+c_fountain);
		// console.log("<<-->> "+c_millstone);
		// console.log("<<-->> "+c_shed);
		// console.log("<<-->> "+c_well);
		// console.log("<<-->> "+c_abbey);
		// console.log("<<-->> "+c_tample);
		// console.log("<<-->> "+c_tavern);
		// console.log("<<-->> "+c2_tavern);
		// console.log("<<-->> "+c_feasthall);
		// console.log("<<-->> "+c_almshouse);
		// console.log("<<-->> "+c2_almshouse);
		// console.log("<<-->> "+c_bakery);
		// console.log("<<-->> "+c_theater);
		// console.log("<<-->> "+c_tailor);
		// console.log("<<-->> "+c_market);
		// console.log("<<-->> "+c_warehouse);
		// console.log("<<-->> "+c_tradingpost);
		// console.log("<<-->> "+c_coc);
		// console.log("<<-->> "+c_asa);

		var tablero = "<center><h1>¡Buena partida!</h1><div><div style ='background-color:grey;width:500px;height:300px;border-radius:3px;border-color:black;border-style:solid;box-shadow:1px 1px 1px 1px black'><br><span class = 'menú3x'>Tabla de puntuaciones</span><hr><br><table id='score'><tr><th>Materiales colocados: </th><th> "+materiales_contador+" </th></tr><tr><th>Edificios construidos: </th><th> "+edificio_contador+" </th></tr><tr><th>Puntos por construcción: </th><th> "+puntoscrudos+" </th></tr><tr><th>Puntos penalizados: </th><th> "+penalización+" </th></tr><tr><th>Puntuación total: </th><th>"+total_puntos+" </th></tr></table><br><span class = 'menú3x'>¡Gracias por jugar!</span></div><br><input type = 'button' onclick='location.reload()' class ='menú3' value = 'Regresar'></input></div></center>";
		document.getElementById("global").innerHTML = tablero;
		console.log("Perdiste luser x2");
}

function Fin(){
	var test1 = document.getElementById("test1").id;
	var test2 = document.getElementById("test2").id;
	var test3 = document.getElementById("test3").id;

	
	
	if (contadorGlobal == "16" && numeroConstrucciones == 0)
	{
		Fin2();
		
	}
}

// final de la partida - codigo


function PuntosFinal(){



	var bandera=0;
	var contadorDeFarm=0;
	var contadorDeGreenhouse=0;
	var puntosCottage=0;
	var puntosShed=0;
	var puntosFountain=0;
	var puntosMillstone=0;
	var puntosShed=0;
	for(var x = 0; x < matrix.length; x++){
		for(var y = 0; y < matrix[x].length; y++){
			if(granary==true){
			if (matrix[x][y]=="Granary") {
				var c = "c";
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+(y-1));
				var id2 = String(c+(x-1)+y);
				var id3 = String(c+(x-1)+(y+1));
				var id4 = String(c+x+(y-1));
				var id5 = String(c+x+(y+1));
				var id6 = String(c+(x+1)+(y-1));
				var id7 = String(c+(x+1)+y);
				var id8 = String(c+(x+1)+(y+1));
				var construccion =document.getElementById(id);
				var construccion1 =document.getElementById(id1);
				var construccion2 =document.getElementById(id2);
				var construccion3 =document.getElementById(id3);
				var construccion4 =document.getElementById(id4);
				var construccion5 =document.getElementById(id5);
				var construccion6 =document.getElementById(id6);
				var construccion7 =document.getElementById(id7);
				var construccion8 =document.getElementById(id8);
				switch (bandera) {
					case 0:if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 1:if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 2:if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 3:if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 4:if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 5:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 6:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 7:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 8:if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 9:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 10:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion8.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 11:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion6.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion7.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 12:if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 13:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 14:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion3.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion5.value=="Cottage"){
						puntosCottage++;
					}
					break;
					case 15:if(construccion1.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion2.value=="Cottage"){
						puntosCottage++;
					}
					if(construccion4.value=="Cottage"){
						puntosCottage++;
					}
					break;
					default:
					break;
				}
			}
			}
			if(farm==true){
				var i = 0;
				var contadorDeCottage = 0;
				if(matrix[x][y]=="Farm"){
					contadorDeFarm++
				}
				while(i < tabla.length){
					if(tabla[i].value=="Cottage"){
						contadorDeCottage++;
					}
					i++;
				}
				if(bandera==15){
				if((contadorDeFarm*4)>=contadorDeCottage){
					puntosCottage=contadorDeCottage;
				}
				else
				{
					while (contadorDeCottage==(contadorDeFarm*4)) {
						contador--;
					}
					puntosCottage=contadorDeCottage;
				}
				}
			}
			if(orchard==true){
				if (matrix[x][y]=="Orchard") {
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+(y-1));
				var construccion =document.getElementById(id);
				for(var i = 0; i < matrix.length; i++){
					if(matrix[i][y]=="Cottage"){
						puntosCottage++;
					}
				}
				for(var a = 0; a < matrix[x].length; a++){
					if(matrix[i][y]=="Cottage"){
						puntosCottage++;
					} 
				   }
				}
			}
			if(greenhouse==true){
				if (matrix[x][y]=="Greenhouse") {
					var i = 0;
					while(i < tabla.length){
						if(tabla[i].value=="Cottage"){
							contadorDeCottage++;
						}
						i++;
					}
				}
				
			}
			if(fountain==true){
				if (matrix[x][y]=="Fountain") {
				var c = "c";
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+y);
				var id2 = String(c+x+(y-1));
				var id3 = String(c+x+(y+1));
				var id4 = String(c+(x+1)+y);
				var construccion =document.getElementById(id);
				var construccion1 =document.getElementById(id1);
				var construccion2 =document.getElementById(id2);
				var construccion3 =document.getElementById(id3);
				var construccion4 =document.getElementById(id4);
				switch (bandera) {
					case 0:if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 1:if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 2:if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 3:if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 4:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 5:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 6:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 7:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 8:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 9:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 10:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 11:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion4.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 12:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 13:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 14:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion3.value=="Fountain"){
						puntosFountain++;
					}
					break;
					case 15:if(construccion1.value=="Fountain"){
						puntosFountain++;
					}
					if(construccion2.value=="Fountain"){
						puntosFountain++;
					}
					break;
					default:
					break;
				}

				}
			}
			if(millstone==true){
				if (matrix[x][y]=="Millstone") {
				var c = "c";
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+y);
				var id2 = String(c+x+(y-1));
				var id3 = String(c+x+(y+1));
				var id4 = String(c+(x+1)+y);
				var construccion =document.getElementById(id);
				var construccion1 =document.getElementById(id1);
				var construccion2 =document.getElementById(id2);
				var construccion3 =document.getElementById(id3);
				var construccion4 =document.getElementById(id4);
				switch (bandera) {
					case 0:if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 1:if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 2:if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 3:if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 4:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 5:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 6:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 7:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 8:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 9:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 10:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 11:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion4.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 12:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 13:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 14:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion3.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					case 15:if(construccion1.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					if(construccion2.value=="Farm"||"Granary"||"Greenhouse"||"Orchard"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosFountain++;
					}
					break;
					default:
					break;
				}

				}
			}
			if(shed==true){
				if(matrix[x][y]){
					puntosShed++;
				}
			}
			if(well==true){
				if (matrix[x][y]=="Well") {
				var c = "c";
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+y);
				var id2 = String(c+x+(y-1));
				var id3 = String(c+x+(y+1));
				var id4 = String(c+(x+1)+y);
				var construccion =document.getElementById(id);
				var construccion1 =document.getElementById(id1);
				var construccion2 =document.getElementById(id2);
				var construccion3 =document.getElementById(id3);
				var construccion4 =document.getElementById(id4);
				switch (bandera) {
					case 0:if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 1:if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 2:if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 3:if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 4:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 5:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 6:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 7:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 8:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 9:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 10:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 11:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion4.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 12:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 13:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 14:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					if(construccion3.value=="Cottage"){
						puntosWell++;
					}
					break;
					case 15:if(construccion1.value=="Cottage"){
						puntosWell++;
					}
					if(construccion2.value=="Cottage"){
						puntosWell++;
					}
					break;
					default:
					break;
				}

				}
			}
			if(abbey==true){
				if (matrix[x][y]=="Abbey") {
				var c = "c";
				var id = String(c+x+y);
				var id1 = String(c+(x-1)+y);
				var id2 = String(c+x+(y-1));
				var id3 = String(c+x+(y+1));
				var id4 = String(c+(x+1)+y);
				var construccion =document.getElementById(id);
				var construccion1 =document.getElementById(id1);
				var construccion2 =document.getElementById(id2);
				var construccion3 =document.getElementById(id3);
				var construccion4 =document.getElementById(id4);
				switch (bandera) {
					case 0:if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					
					}
					else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosAbbey++;
					}
					break;
					case 1:if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 2:if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 3:if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosAbbey++;
					}
					break;
					case 4:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 5:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 6:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 7:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 8:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 9:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 10:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 11:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion4.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 12:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosAbbey++;
					}
					break;
					case 13:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 14:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion3.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					break;
					case 15:if(construccion1.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
					}else
					{
						puntosAbbey++;
					}
					if(construccion2.value=="Almshouse"||"Feast Hall"||"Tavern"||"Factory"||"Trading Post"||"Waerehouse"||"Bakery"||"Market"||"Tailor"||"Theater"){
						puntosAbbey++;
					}
					break;
					default:
					break;
				}

				}
			}
			if(almshouse == true){
			if(matrix[x][y] == "Almshouse"){
			 				contadorAlmshouse++;
			 	switch (contadorAlmshouse) {
			 		case 1: puntosAlmshouse = -1;
			 		break;
			 		case 2: puntosAlmshouse = 5;
			 		break;
			 		case 3: puntosAlmshouse = -3;
			 		break;
			 		case 4: puntosAlmshouse = 15;
			 		break;
			 		case 5: puntosAlmshouse = -5;
			 		break;
			 		case 6: puntosAlmshouse = 26;
			 		break;
				}
			}
			}
			if(cloister=true){
				if(matrix[x][y]=="Cloister"){
					if(x==0||3 && y==0||3){
						puntosCloinster++;
						puntosCloinster++;
						puntosCloinster++;
						puntosCloinster++;
					}
					else
					{
						puntosCloinster++;
					}

				}
			}

			bandera++;
		}
 	}
	PuntosTotales=(puntosCottage*3)+(puntosFountain*2)+(puntosMillstone*2)+(puntosWell*2)+(puntosShed*2)
}